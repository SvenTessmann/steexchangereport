﻿#requires -Version 1
# Build 3.0.15.4.13
Function Get-STEExchangeVersionByRegistry ()
{
  $ExVersion = (Get-ChildItem -Path HKLM:\SOFTWARE\Microsoft\ExchangeServer\v* -ErrorAction SilentlyContinue).PSChildName
  Return $ExVersion
}
