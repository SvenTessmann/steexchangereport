﻿#requires -Version 2
# Build 3.0.15.4.13
Function Connect-STEExchangeServer
{
  Param
  (
    [System.Object]
    $ExchangeServer
  )
  $ProgressPreference = 'SilentlyContinue'
  $ExchangeSession    = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$ExchangeServer/PowerShell/ -Authentication Kerberos
  $null               = Import-PSSession -Session $ExchangeSession -DisableNameChecking
}
