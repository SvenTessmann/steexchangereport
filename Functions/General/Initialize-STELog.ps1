﻿#requires -Version 1
# Build 3.0.15.4.13
Function Start-STELog
{
  If($gintLogLevel -ge 3)
  {
    $LogLine  = '[INF];'
    $LogLine += Get-Date -Format 'yyyy.MM.dd;hh:mm:ss;'
    $LogLine += '################################################ START ################################################'
    Add-Content -Path $gstrLogFile -Value $LogLine
  }
}

Function Close-STELog
{
  If($gintLogLevel -ge 3)
  {
    $LogLine  = '[INF];'
    $LogLine += Get-Date -Format 'yyyy.MM.dd;hh:mm:ss;'
    $LogLine += '################################################  END  ################################################'
    Add-Content -Path $gstrLogFile -Value $LogLine
  }
}

Function Write-STELog
{
  Param
  (
    [Parameter(Mandatory)]
    [ValidateSet('ERROR','WARN','INFO','SUCCESS','DEBUG')]
    $Level,
    
    [Parameter(Mandatory)]
    $Value
  )

  If($Level -eq 'ERROR')
  {
    $LogLevel = 1
    $LogLine  = '[ERR];'
    $Color    = 'Red'
  }

  ElseIf($Level -eq 'WARN')
  {
    $LogLevel = 2
    $LogLine  = '[WAR];'
    $Color    = 'Yellow'
  }

  ElseIf($Level -eq 'INFO')
  {
    $LogLevel = 3
    $LogLine  = '[INF];'
    $Color    = 'White'
  }
  
  ElseIf($Level -eq 'SUCCESS')
  {
    $LogLevel = 4
    $LogLine  = '[SUC];'
    $Color    = 'Green'
  }
  
  ElseIf($Level -eq 'DEBUG')
  {
    $LogLevel = 5
    $LogLine  = '[DEB];'
    $Color    = 'Cyan'
  }
  
  If($LogLevel -le $gintLogLevel)
  {
    $LogLine += Get-Date -Format 'yyyy.MM.dd;hh:mm:ss;'
    $LogLine += $Value

    Add-Content -Path $gstrLogFile -Value $LogLine
    Write-Host -Object $Value -ForegroundColor $Color
  }
}
