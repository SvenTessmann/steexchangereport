﻿#requires -Version 1
# Build 3.0.15.4.13
Function Read-STEIniFile
# http://blogs.technet.com/b/heyscriptingguy/archive/2011/08/20/use-powershell-to-work-with-any-hashIniContent-file.aspx
{
  Param
  (
    [System.Object]
    $Path
  )
  [hashtable]$hashIniContent = @{}
  Switch -Regex -File $Path
  {
    '^\[(.+)\]' # Section
    {
      $Section = $Matches[1]
      $hashIniContent[$Section] = @{}
      [int]$CommentCount = 0
    }
    "^(;.*)$" # Comment
    {
      $Value = $Matches[1]
      $CommentCount = $CommentCount + 1
      $Name = 'Comment' + $CommentCount
      $hashIniContent[$Section][$Name] = $Value
    } 
    '(.+?)\s*=(.*)' # Key
    {
      $Name, $Value = $Matches[1..2]
      $hashIniContent[$Section][$Name] = $Value
    }
  }
  Return $hashIniContent
}
