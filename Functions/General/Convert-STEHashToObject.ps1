﻿#requires -Version 1
# Build 3.0.15.4.13
Function Convert-STEHashToObject
{
  Param
  (
    [System.Object]
    $HashTable
  )

  $NewObject = @()
  ForEach ($Key in $HashTable.Keys)
  {
    $TempObj = New-Object -TypeName PSObject
    Add-Member -InputObject $TempObj -Name 'Setting' -Value $Key -MemberType NoteProperty
    Add-Member -InputObject $TempObj -Name 'Value' -Value $HashTable.$Key -MemberType NoteProperty
    $NewObject += $TempObj
  }
  return $NewObject
}
