﻿#requires -Version 1
# Build 3.0.15.4.13
Function New-STEMainReport
# Common HTML header, styles and title
{
  Param
  (
    [System.Object]
    $Title
  )
  $strHTMLMainReportHeader = "
    <html>
    <style>
    BODY{font-family: Arial; font-size: 10pt;}
    H1{font-size: 18px;}
    H2{font-size: 14px;}
    H3{font-size: 12px;}
    TABLE{border: 1px solid black; border-collapse: collapse; font-size: 8pt;}
    TH{border: 1px solid black; background: #F2F2F2; padding: 5px; color: #000000;}
    TD{border: 1px solid black; background: #FFFFFF; padding: 5px; color: #000000;}
    td.ok{background: #C5F0CF; color: #006242;}
    td.warning{background: #FEEBA1; color: #9C6436;}
    td.error{background: #FFC6CE; color: #9D0041;}
    </style>
    <body>
    <h1 align=""center"">$Title</h1>
    <h3 align=""center"">Generated: $gdtmScriptStart on $gstrScriptServer</h3>
  "
  Return $strHTMLMainReportHeader
}

Function New-STEModuleReport
{
  Param
  (
    [System.Object]
    $Title
  )
  $strHTMLModuleReport = "
    <HR>
    <h2 align=""center"">$Title</h2>
  "
  Return $strHTMLModuleReport
}

Function New-STEReportFooter
{
  $strHTMLReportFooter = "
    <HR>
    <font color=""#9A9A9A"">Report Version $gstrScriptVersion<br />
    Written By $gstrScriptAuthor for $gstrCustomer<br />
    Loaded modules:<br />
    <ul>
  "
  Return $strHTMLReportFooter
}

Function New-STEReportTail
{
  $strHTMLReportTail = '
    </ul>
    </font>
    </body>
    </html>
  '
  Return $strHTMLReportTail
}
