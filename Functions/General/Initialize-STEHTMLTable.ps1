﻿#requires -Version 1
# Build 3.0.15.04.13
Function New-STEHTMLTable
{
  Param
  (
    [System.Object]
    $Title,

    [System.Object]
    $Header
  )

  $NewHTMLTable = "
    <h3>$Title</h3>
    <table>
    <tr>
  "
  ForEach ($Cell in $Header)
  {
    $NewHTMLTable += "<th>$Cell</th>"
  }
  $NewHTMLTable += '</tr>'
  Return $NewHTMLTable
}

Function New-STEHTMLTableRow
{
  Param
  (
    [System.Object]
    $Cells
  )
  
  $NewTableRow = '<tr>'
  ForEach ($Cell in $Cells)
  {
    If ($Cell.Status -eq 'error')
    {
      $NewTableRow += "<td class=""error"">$($Cell.Value)</td>"
    }
    ElseIf ($Cell.Status -eq 'warning')
    {
      $NewTableRow += "<td class=""warning"">$($Cell.Value)</td>"
    }
    ElseIf ($Cell.Status -eq 'ok')
    {
      $NewTableRow += "<td class=""ok"">$($Cell.Value)</td>"
    }
    Else
    {
      $NewTableRow += "<td>$($Cell.Value)</td>"
    }
  }
  $NewTableRow += '</tr>'
  Return $NewTableRow
}

Function New-STEHTMLTableLine 
{
  Param
  (
    [System.Object]
    $Cells
  )

  $NewTableLine = '<tr>'
  ForEach ($Cell in $Cells)
  {
    $NewTableLine += "<td>$Cell</td>"
  }
  $NewTableLine += '</tr>'
  Return $NewTableLine
}

Function New-STEHTMLTableTail
{
  $NewTableTail = '</table>'
  Return $NewTableTail
}
