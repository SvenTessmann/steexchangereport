﻿#requires -Version 1
# Build 3.0.15.4.13
Function New-STEHTMLInlinePicture
# http://winsysadm.net/send-mail-with-inline-embedded-images-with-powershell/
{
  Param
  (
    [System.Object]
    $PicturePath
  )

  $Pictures = Get-ChildItem -Path "$PicturePath" -Name
  Foreach ($Picture in $Pictures) 
  {
    $HTMLLine += "<img src=`"cid:$Picture`" alt=`"picture`">"
  }
  Return $HTMLLine
}
