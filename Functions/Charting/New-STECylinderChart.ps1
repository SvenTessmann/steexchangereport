﻿#requires -Version 1
# Build 3.0.15.4.13
Function New-STECylinderChart
{
  Param
  (
    [System.Object]
    $Title,
    
    [System.Object]
    $Width,

    [System.Object]
    $Height,

    [System.Object]
    $Xtitle,

    [System.Object]
    $Ytitle,

    [System.Object]
    $Data,

    [System.Object]
    $ImagePath
  )

  # Create chart object
  $Chart = New-Object -TypeName System.Windows.Forms.DataVisualization.Charting.Chart 
  $Chart.Width  = $Width
  $Chart.Height = $Height
  $Chart.Left   = 40
  $Chart.Top    = 30

  # Create a chartarea to draw on and add to chart
  $ChartArea = New-Object -TypeName System.Windows.Forms.DataVisualization.Charting.ChartArea 
  $Chart.ChartAreas.Add($ChartArea)
    
  # Add title and axes labels
  [void]$Chart.Titles.Add("$Title")
  $ChartArea.AxisX.Title    = $Xtitle 
  $ChartArea.AxisX.Interval = 1
  $ChartArea.AxisY.Title    = $Ytitle

  # Add Data to chart
  [void]$Chart.Series.Add('Data')

  # Make bars into 3d cylinders
  $Chart.Series['Data']['DrawingStyle'] = 'Cylinder'
  $Chart.Series['Data'].Points.DataBindXY($Data.Keys, $Data.Values)

  # Save chart to file
  $Chart.SaveImage("$ImagePath", 'PNG')
}
