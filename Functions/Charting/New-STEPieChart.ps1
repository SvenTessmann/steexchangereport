﻿#requires -Version 1
# Build 3.0.15.4.13
Function New-STEPieChart 
{
  Param
  (
    [System.Object]
    $Title,

    [System.Object]
    $Width,

    [System.Object]
    $Height,

    [System.Object]
    $Data,

    [System.Object]
    $ImagePath
  )

  # Create chart object
  $Chart = New-Object -TypeName System.Windows.Forms.DataVisualization.Charting.Chart 
  $Chart.Width  = $Width
  $Chart.Height = $Height
  $Chart.Left   = 40 
  $Chart.Top    = 30
    
  # Create a chartarea to draw on and add to chart
  $ChartArea = New-Object -TypeName System.Windows.Forms.DataVisualization.Charting.ChartArea 
  $Chart.ChartAreas.Add($ChartArea)
    
  # Add title
  [void]$Chart.Titles.Add("$Title")
    
  # Add Data to chart
  [void]$Chart.Series.Add('Data') 
  $Chart.Series['Data'].Points.DataBindXY($Data.Keys, $Data.Values)
    
  # Set chart type
  $Chart.Series['Data'].ChartType = [System.Windows.Forms.DataVisualization.Charting.SeriesChartType]::Pie
    
  # Set chart options
  $Chart.Series['Data']['PieLabelStyle']   = 'Inside'
  $Chart.Series['Data']['PieLineColor']    = 'Black'
  $Chart.Series['Data']['PieDrawingStyle'] = 'SoftEdge'
  $Chart.Series['Data']['PieStartAngle']   = '270'

  # Save chart to file
  $Chart.SaveImage("$ImagePath", 'PNG')
}
