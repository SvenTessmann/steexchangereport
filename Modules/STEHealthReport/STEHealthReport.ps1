﻿# History:
# 3.0.15.7.9: Changed $mobjServer.Identity to $mobjServer.Fqdn to resolve problems with NetBIOS name resolution.
#             Change $marrServiceHealth
#.............Removed mail flow test, because it wasn´t meaningful or has problems with Exchange 2013 remotly

#region MODULE INFORMATION
################################
# MODULE INFORMATION (Universal)
################################
# Module version
[string]$mstrModuleVersion = '3.0 (Build: 3.0.15.7.9)'

# Get module start time
[datetime]$mdtmModuleStart = Get-Date
Write-STELog -Level INFO -Value "Module $gobjModule started!"
#endregion

#region INI-FILE
###############
# READ INI-FILE
###############
Try
{
  Write-STELog -Level DEBUG -Value 'Reading .ini file...'
  [hashtable]$mhashIniContent = Read-STEIniFile -Path "$gstrModulePath\$gobjModule\Settings.ini"
  Write-STELog -Level DEBUG -Value "$gobjModule.ini loaded!"
}
Catch
{
  Write-STELog -Level ERROR -Value 'Error reading .ini file'
  Write-STELog -Level ERROR -Value $Error[0]
}

# Section [ModuleSettings]
[string]$mstrTitle                  = $mhashIniContent['ModuleSettings']['Title']
[int]$mintTransportQueueWarning     = $mhashIniContent['ModuleSettings']['TransportQueueWarning']
[int]$mintTransportQueueError       = $mhashIniContent['ModuleSettings']['TransportQueueError']
[int]$mintMapiTimeout               = $mhashIniContent['ModuleSettings']['MapiTimeout']
[string]$mstrExcludedExchangeServer = $mhashIniContent['ModuleSettings']['ExcludedServer']
# Convert string into array
[array]$marrExcludedExchangeServer  = $mstrExcludedExchangeServer.Split(',')
#endregion

#region IMPORT DATA
#############
# IMPORT DATA
#############
# Get all Exchange server
[array]$marrAllExchangeServer = @(Get-ExchangeServer | Sort-Object -Property Site, Name)
# If a list of excluded mailbox databases exists, remove them from selection
If ($marrExcludedExchangeServer.Count -gt '0')
{
  [array]$marrExchangeServer = @()
  ForEach ($mobjServer in $marrAllExchangeServer)
  {
    If ($marrExcludedExchangeServer -notcontains $mobjServer)
    {
      $marrExchangeServer += $mobjServer
    }
  }
}
Else
{
  $marrExchangeServer = $marrAllExchangeServer
}
#endregion

#region START REPORT
##############
# START REPORT
##############
# Add report title
[string]$mstrReportData = New-STEModuleReport -Title $mstrTitle
# Module summary report
[array]$marrReportSummaryData = @()

# New table
[string]$mstrTableName  = 'Exchange Server Health'
[array]$marrTableHeader = @('Server', 'Site', 'Roles', 'Version', 'DNS', 'Ping', 'Uptime (hrs)', 'CAS Services', 'HT Services', 'MBX Services', 'UM Services', 'Transport Queue', 'PF DBs Mounted', 'MBX DBs Mounted', 'MAPI Test')
$mstrReportData        += New-STEHTMLTable -Title $mstrTableName -Header $marrTableHeader
#endregion

#region PROCESS
#########
# PROCESS
#########
# Get information per server
ForEach ($mobjServer in $marrExchangeServer)
{
  # SITE
  # Skip site attribute for Exchange 2003 server
  If ($mobjServer.AdminDisplayVersion -like 'Version 6.*')
  {
    [string]$mstrSite = 'n/a'
  }
  Else
  {
    [string]$mstrSite = ($mobjServer.Site.ToString()).Split('/')[-1]
  }

  # ROLES
  [string]$mstrRoles = $mobjServer.ServerRole
  # Convert string to array
  [array]$marrRoles  = $mstrRoles.Split(',').Trim()

  # VERSION
  [string]$mstrVersion = $mobjServer.AdminDisplayVersion
  If ($mstrVersion -like 'Version 6.*')
  {
    $mstrVersion = 'Exchange 2003'
  }
  If ($mstrVersion -like 'Version 8.*')
  {
    $mstrVersion = 'Exchange 2007'
  }
  If ($mstrVersion -like 'Version 14.*')
  {
    $mstrVersion = 'Exchange 2010'
  }
  If ($mstrVersion -like 'Version 15.*')
  {
    $mstrVersion = 'Exchange 2013'
  }

  # DNS
  Try
  {
    $mobjIP = @([System.Net.Dns]::GetHostByName($mobjServer.Fqdn).AddressList | Select-Object -Property IPAddressToString -ExpandProperty IPAddressToString)
  }
  Catch
  {
    $mobjIP = $null
  }
  If ($mobjIP -ne $null)
  {
    [string]$mstrDNS = 'Pass'
    
    # PING
    [array]$marrPing = $null
    Try
    {
      [bool]$marrPing = Test-Connection -ComputerName $mobjServer.Fqdn -Quiet -ErrorAction Stop
    }
    Catch
    {
      Write-Host -ForegroundColor Red -Object $_.Exception.Message
    }
    Switch ($marrPing)
    {
      $true
      {
        [string]$mstrPing = 'Pass'
      }
      default
      {
        [string]$mstrPing       = 'Fail'
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjServer.Identity)</strong></font> - Ping failed"
      }
    }
  }
  Else
  {
    [string]$mstrDNS        = 'Fail'
    $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjServer.Identity)</strong></font> - Server not found in DNS"
    [string]$mstrPing       = 'Fail'
    $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjServer.Identity)</strong></font> - Ping failed"
  }
  
  # UPTIME
  # Exchange 2003 needs permissions to remote query WMI
  If ($mobjServer.AdminDisplayVersion -like 'Version 6.*')
  {
    # Uptime - Exchange 2003
    [int]$mintUptime = $null
    $mobjOS          = $null
    Try 
    {
      $mobjOS = Get-WmiObject -Class Win32_OperatingSystem -ComputerName $mobjServer.Fqdn -ErrorAction SilentlyContinue
    }
    Catch
    {
      Write-Host -ForegroundColor Red -Object $_.Exception.Message
    }
    If ($mobjOS -eq $null)
    {
      [string]$mintUptime     = 'Unable to retrieve uptime'
      $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Unable to retrieve uptime"
    }
    Else
    {
      $mobjUptime = $mobjOS.ConvertToDateTime($mobjOS.LocalDateTime) - $mobjOS.ConvertToDateTime($mobjOS.LastBootUpTime)
      [int]$mintUptime = '{0:00}' -f $mobjUptime.TotalHours
      If ($mintUptime -lt 24)
      {
        $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Uptime is less than 24 hours"
      }
    }
  }
  # Exchange 2010/13 uses remote eventlog query, need to be member of local group "Event Log Reader" (less permissions than Exchange 2003)
  Else
  {
    [int]$mintUptime          = $null
    $mobjEventLogSystemUptime = $null
    Try 
    {
      $mobjEventLogSystemUptime         = Get-WinEvent -ComputerName $mobjServer.Fqdn -FilterHashtable @{
        Logname = 'System'
        ID      = 6013
      } -MaxEvents 1 | Select-Object -Property TimeCreated, Message -ErrorAction SilentlyContinue
      [int]$mintSystemUptimeSeconds     = $mobjEventLogSystemUptime.Message.Split(' ')[-2]
      [datetime]$mdtmSytemUptimeDate    = $mobjEventLogSystemUptime.TimeCreated
      [datetime]$mdtmSystemLastBootTime = $mdtmSytemUptimeDate.AddSeconds(-$mintSystemUptimeSeconds)
    }
    Catch
    {
      Write-Host -ForegroundColor Red -Object $_.Exception.Message
    }
    If ($mobjEventLogSystemUptime -eq $null)
    {
      [string]$mintUptime     = 'Unable to retrieve uptime'
      $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Unable to retrieve uptime"
    }
    Else
    {
      [int]$mintUptime = [math]::Round(($mdtmModuleStart-$mdtmSystemLastBootTime).TotalHours)
      If ($mintUptime -lt 24)
      {
        $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Uptime is less than 24 hours"
      }
    }
  }
  
  # Setting default values
  [string]$mstrCASservices                 = 'n/a'
  [string]$mstrHTservices                  = 'n/a'
  [string]$mstrMBXservices                 = 'n/a'
  [string]$mstrUMservices                  = 'n/a'
  [string]$mstrTransportQueue              = 'n/a'
  [string]$mstrPublicFolderDatabaseMounted = 'n/a'
  [string]$mstrMailboxDatabaseMounted      = 'n/a'
  [string]$mstrMAPITest                    = 'n/a'
  [array]$marrServiceHealth = @()
  Try 
  {
    # SERVICES
    # Service health is an array due to how multi-role servers return Test-ServiceHealth status
    $marrServiceHealth = @(Test-ServiceHealth -Server $mobjServer.Fqdn -ErrorAction Stop)
  }
  Catch 
  {
    # Services for Exchage 2003 cannot be tested with this commandlet
    If ($mobjServer.AdminDisplayVersion -like 'Version 6.*')
    {
      $mstrCASservices = 'n/a'
      $mstrHTservices  = 'n/a'
      $mstrMBXservices = 'n/a'
      $mstrUMservices  = 'n/a'
    }
    Else
    {
      $mstrCASservices        = 'Could not test service health'
      $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Could not test service health"
      $mstrHTservices         = 'Could not test service health'
      $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Could not test service health"
      $mstrMBXservices        = 'Could not test service health'
      $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Could not test service health"
      $mstrUMservices         = 'Could not test service health'
      $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Could not test service health"
    }
  }
  If ($marrServiceHealth)
  {
    ForEach($mobjService in $marrServiceHealth)
    {
      Switch ($mobjService.RequiredServicesRunning)
      {
        $true
        {
          [string]$mstrRoleName = $mobjService.Role
          If ($mstrRoleName -match 'Client Access')
          {
            $mstrCASservices = 'Pass'
          }
          ElseIf ($mstrRoleName -match 'Hub Transport')
          {
            $mstrHTservices = 'Pass'
          }
          ElseIf ($mstrRoleName -match 'Mailbox')
          {
            $mstrMBXservices = 'Pass'
          }
          ElseIf ($mstrRoleName -match 'Unified Messaging')
          {
            $mstrUMservices = 'Pass'
          }
        }
        $false
        {
          [string]$mstrRoleName = $mobjService.Role
          If ($mstrRoleName -match 'Client Access')
          {
            $mstrCASservices = 'Fail'
          }
          ElseIf ($mstrRoleName -match 'Hub Transport')
          {
            $mstrHTservices = 'Fail'
          }
          ElseIf ($mstrRoleName -match 'Mailbox')
          {
            $mstrMBXservices = 'Fail'
          }
          ElseIf ($mstrRoleName -match 'Unified Messaging')
          {
            $mstrUMservices = 'Fail'
          }
          $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjServer.Identity)</strong></font> - $mstrRoleName required services not running"
        }
      }
    }
  }

  # TRANSPORT QUEUE
  If ($mobjServer.IsHubTransportServer -eq $true)
  {
    [array]$marrQueue = $null
    Try 
    {
      $marrQueue = Get-Queue -Server $mobjServer.Fqdn -ErrorAction Stop
    }
    Catch 
    {
      $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Could not check queue"
    }
    If ($marrQueue)
    {
      $mobjQueueCount      = $marrQueue | Measure-Object -Property MessageCount -Sum
      [int]$mintQueueCount = $mobjQueueCount.sum
      If ($mintQueueCount -le $mintTransportQueueWarning)
      {
        [string]$mstrTransportQueue = 'Pass'
      }
      ElseIf ($mintQueueCount -gt $mintTransportQueueWarning -and $mintQueueCount -lt $mintTransportQueueError)
      {
        [string]$mstrTransportQueue = 'Warn'
        $marrReportSummaryData     += "<font color=#9C6436><strong>$($mobjServer.Identity)</strong></font> - Transport queue is above warning threshold" 
      }
      Else
      {
        [string]$mstrTransportQueue = 'Fail'
        $marrReportSummaryData     += "<font color=#9D0041><strong>$($mobjServer.Identity)</strong></font> - Transport queue is above high threshold"
      }
    }
    Else
    {
      [string]$mstrTransportQueue = 'Unknown'
    }
  }
  
  # DATABASES
  If ($mobjServer.IsMailboxServer -eq $true)
  {
    # Get all public folder and mailbox databases
    Try
    {
      [array]$marrAllPublicFolderDatabases = @(Get-PublicFolderDatabase -Server $mobjServer.Fqdn -Status )
      If ($marrAllPublicFolderDatabases.Count -eq 0)
      {
        Write-STELog -Level DEBUG -Value "$($mobjServer.Identity) has no Public Folder Database"
      }
      Else
      {
        Write-STELog -Level DEBUG -Value "$($mobjServer.Identity) Public Folder Database found"
      }
    }
    Catch
    {
      Write-STELog -Level DEBUG -Value "$($mobjServer.Identity) has no Public Folder Database"
      Write-STELog -Level DEBUG -Value $Error[0]
    }
    Try
    {
      [array]$marrAllMailboxDatabases     = @(Get-MailboxDatabase -Server $mobjServer.Fqdn -Status | Where-Object -FilterScript {
          $_.Recovery -ne $true
        }
      )
      [array]$marrMountedMailboxDatabases = @(Get-MailboxDatabase -Server $mobjServer.Fqdn -Status | Where-Object -FilterScript {
          $_.Recovery -ne $true -and $_.MountedOnServer -eq ($mobjServer.Fqdn)
        }
      )
      Write-STELog -Level DEBUG -Value "$($mobjServer.Identity) Mailbox Database found"
    }
    Catch
    {
      Write-STELog -Level DEBUG -Value "$($mobjServer.Identity) has no Mailbox Database"
      Write-STELog -Level DEBUG -Value $Error[0]
    }
      
    # DATABASE MOUNTED
    # Check public folder databases
    If ($marrAllPublicFolderDatabases.Count -gt 0)
    {
      [string]$mstrPublicFolderDatabaseMounted = 'Pass'
      [array]$marrAlertDatabases               = @()
      ForEach ($mobjDatabase in $marrAllPublicFolderDatabases)
      {
        If (($mobjDatabase.Mounted) -ne $true)
        {
          $mstrPublicFolderDatabaseMounted = 'Fail'
          $marrAlertDatabases             += $mobjDatabase.Name
        }
      }
      If ($marrAlertDatabases.Count -ne 0)
      {
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjServer.Identity)</strong></font> - Public Folder database not mounted"
      }
    }
    # Check mailbox databases
    If ($marrAllMailboxDatabases.Count -gt 0)
    {
      [string]$mstrMailboxDatabaseMounted = 'Pass'
      [array]$marrAlertDatabases          = @()
      ForEach ($mobjDatabase in $marrAllMailboxDatabases)
      {
        If (($mobjDatabase.Mounted) -ne $true)
        {
          [string]$mstrMailboxDatabaseMounted = 'Fail'
          $marrAlertDatabases                += $mobjDatabase.Name
        }
      }
      If ($marrAlertDatabases.Count -ne 0)
      {
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjServer.Identity)</strong></font> - Mailbox databases not mounted"
      }
    }
  }
  
  # MAPI TEST
  If ($mobjServer.IsMailboxServer -eq $true)
  {
    [string]$mstrMAPITest      = 'Unknown'
    [array]$marrAlertDatabases = @()
    ForEach ($mobjDatabase in $marrAllMailboxDatabases)
    {
      $mobjMAPIStatus = Test-MAPIConnectivity -Database $mobjDatabase.Identity -PerConnectionTimeout $mintMapiTimeout
      If ($mobjMAPIStatus.Result.Value -eq $null)
      {
        $mstrMAPITest = $mobjMAPIStatus.Result
      }
      Else
      {
        $mstrMAPITest = $mobjMAPIStatus.Result.Value
      }
      If (($mstrMAPITest) -ne 'Success')
      {
        $mstrMAPITest = 'Fail'
        $marrAlertDatabases += $mobjDatabase.name
      }
    }
    If ($marrAlertDatabases.count -ne 0)
    {
      $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjServer.Identity)</strong></font> - MAPI tests failed"
    }
  }
  
  # Create custom object to store information
  $mobjServerHealth = [pscustomobject]@{
    Name                        = $mobjServer.Name
    Site                        = $mstrSite
    Roles                       = $marrRoles
    Version                     = $mstrVersion
    DNS                         = $mstrDNS
    Ping                        = $mstrPing
    Uptime                      = $mintUptime
    CASservices                 = $mstrCASservices
    HTservices                  = $mstrHTservices
    MBXservices                 = $mstrMBXservices
    UMservices                  = $mstrUMservices
    TransportQueue              = $mstrTransportQueue
    PublicFolderDatabaseMounted = $mstrPublicFolderDatabaseMounted
    MailboxDatabaseMounted      = $mstrMailboxDatabaseMounted
    MAPITest                    = $mstrMAPITest
  }
    
  # Create custom objects for table rows
  # Server
  $mobjtblServer = [pscustomobject]@{
    Value  = $mobjServerHealth.Name
    Status = 'none'
  }
  # Site
  $mobjtblSite = [pscustomobject]@{
    Value  = $mobjServerHealth.Site
    Status = 'none'
  }
  # Roles
  $mobjtblRoles = [pscustomobject]@{
    Value  = $mobjServerHealth.Roles
    Status = 'none'
  }
  # Version
  $mobjtblVersion = [pscustomobject]@{
    Value  = $mobjServerHealth.Version
    Status = 'none'
  }
  # DNS
  If ($mobjServerHealth.DNS -eq 'Pass')
  {
    $mobjtblDNS = [pscustomobject]@{
      Value  = $mobjServerHealth.DNS
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.DNS -eq 'Fail')
  {
    $mobjtblDNS = [pscustomobject]@{
      Value  = $mobjServerHealth.DNS
      Status = 'error'
    }
  }
  Else
  {
    $mobjtblDNS = [pscustomobject]@{
      Value  = $mobjServerHealth.DNS
      Status = 'none'
    }
  }
  # PING
  If ($mobjServerHealth.Ping -eq 'Pass')
  {
    $mobjtblPing = [pscustomobject]@{
      Value  = $mobjServerHealth.Ping
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.Ping -eq 'Fail')
  {
    $mobjtblPing = [pscustomobject]@{
      Value  = $mobjServerHealth.Ping
      Status = 'error'
    }
  }
  Else
  {
    $mobjtblPing = [pscustomobject]@{
      Value  = $mobjServerHealth.Ping
      Status = 'none'
    }
  }
  # UPTIME
  If ($mobjServerHealth.Uptime -is [string])
  {
    $mobjtblUptime = [pscustomobject]@{
      Value  = $mobjServerHealth.Uptime
      Status = 'warning'
    }
  }
  ElseIf ($mobjServerHealth.Uptime -lt 24)
  {
    $mobjtblUptime = [pscustomobject]@{
      Value  = $mobjServerHealth.Uptime
      Status = 'warning'
    }
  }
  Else
  {
    $mobjtblUptime = [pscustomobject]@{
      Value  = $mobjServerHealth.Uptime
      Status = 'ok'
    }
  }
  # CAS SERVICE
  If ($mobjServerHealth.CASservices -eq 'Pass')
  {
    $mobjtblCAS = [pscustomobject]@{
      Value  = $mobjServerHealth.CASservices
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.CASservices -eq 'Fail')
  {
    $mobjtblCAS = [pscustomobject]@{
      Value  = $mobjServerHealth.CASservices
      Status = 'error'
    }
  }
  ElseIf ($mobjServerHealth.CASservices -eq 'n/a')
  {
    $mobjtblCAS = [pscustomobject]@{
      Value  = $mobjServerHealth.CASservices
      Status = 'none'
    }
  }
  Else
  {
    $mobjtblCAS = [pscustomobject]@{
      Value  = $mobjServerHealth.CASservices
      Status = 'warning'
    }
  }
  # HT SERVICE
  If ($mobjServerHealth.HTservices -eq 'Pass')
  {
    $mobjtblHT = [pscustomobject]@{
      Value  = $mobjServerHealth.HTservices
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.HTservices -eq 'Fail')
  {
    $mobjtblHT = [pscustomobject]@{
      Value  = $mobjServerHealth.HTservices
      Status = 'error'
    }
  }
  ElseIf ($mobjServerHealth.HTservices -eq 'n/a')
  {
    $mobjtblHT = [pscustomobject]@{
      Value  = $mobjServerHealth.HTservices
      Status = 'none'
    }
  }
  Else
  {
    $mobjtblHT = [pscustomobject]@{
      Value  = $mobjServerHealth.HTservices
      Status = 'warning'
    }
  }
  # MBX SERVICE
  If ($mobjServerHealth.MBXservices -eq 'Pass')
  {
    $mobjtblMBX = [pscustomobject]@{
      Value  = $mobjServerHealth.MBXservices
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.MBXservices -eq 'Fail')
  {
    $mobjtblMBX = [pscustomobject]@{
      Value  = $mobjServerHealth.MBXservices
      Status = 'error'
    }
  }
  ElseIf ($mobjServerHealth.MBXservices -eq 'n/a')
  {
    $mobjtblMBX = [pscustomobject]@{
      Value  = $mobjServerHealth.MBXservices
      Status = 'none'
    }
  }
  Else
  {
    $mobjtblMBX = [pscustomobject]@{
      Value  = $mobjServerHealth.MBXservices
      Status = 'warning'
    }
  }
  # UM SERVICE
  If ($mobjServerHealth.UMservices -eq 'Pass')
  {
    $mobjtblUM = [pscustomobject]@{
      Value  = $mobjServerHealth.UMservices
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.UMservices -eq 'Fail')
  {
    $mobjtblUM = [pscustomobject]@{
      Value  = $mobjServerHealth.UMservices
      Status = 'error'
    }
  }
  ElseIf ($mobjServerHealth.UMservices -eq 'n/a')
  {
    $mobjtblUM = [pscustomobject]@{
      Value  = $mobjServerHealth.UMservices
      Status = 'none'
    }
  }
  Else
  {
    $mobjtblUM = [pscustomobject]@{
      Value  = $mobjServerHealth.UMservices
      Status = 'warning'
    }
  }
  # TRANSPORT QUEUE
  If ($mobjServerHealth.TransportQueue -eq 'Pass')
  {
    $mobjtblQueue = [pscustomobject]@{
      Value  = $mobjServerHealth.TransportQueue
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.TransportQueue -eq 'Fail')
  {
    $mobjtblQueue = [pscustomobject]@{
      Value  = $mobjServerHealth.TransportQueue
      Status = 'error'
    }
  }
  Else
  {
    $mobjtblQueue = [pscustomobject]@{
      Value  = $mobjServerHealth.TransportQueue
      Status = 'none'
    }
  }
  # PUBLIC FOLDER
  If ($mobjServerHealth.PublicFolderDatabaseMounted -eq 'Pass')
  {
    $mobjtblPF = [pscustomobject]@{
      Value  = $mobjServerHealth.PublicFolderDatabaseMounted
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.PublicFolderDatabaseMounted -eq 'Fail')
  {
    $mobjtblPF = [pscustomobject]@{
      Value  = $mobjServerHealth.PublicFolderDatabaseMounted
      Status = 'error'
    }
  }
  Else
  {
    $mobjtblPF = [pscustomobject]@{
      Value  = $mobjServerHealth.PublicFolderDatabaseMounted
      Status = 'none'
    }
  }
  # MAILBOX DBs
  If ($mobjServerHealth.MailboxDatabaseMounted -eq 'Pass')
  {
    $mobjtblMBX = [pscustomobject]@{
      Value  = $mobjServerHealth.MailboxDatabaseMounted
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.MailboxDatabaseMounted -eq 'Fail')
  {
    $mobjtblMBX = [pscustomobject]@{
      Value  = $mobjServerHealth.MailboxDatabaseMounted
      Status = 'error'
    }
  }
  Else
  {
    $mobjtblMBX = [pscustomobject]@{
      Value  = $mobjServerHealth.MailboxDatabaseMounted
      Status = 'none'
    }
  }
  # MAPI TEST
  If ($mobjServerHealth.MAPITest -eq 'Success')
  {
    $mobjtblMAPI = [pscustomobject]@{
      Value  = $mobjServerHealth.MAPITest
      Status = 'ok'
    }
  }
  ElseIf ($mobjServerHealth.MAPITest -eq 'Fail')
  {
    $mobjtblMAPI = [pscustomobject]@{
      Value  = $mobjServerHealth.MAPITest
      Status = 'error'
    }
  }
  Else
  {
    $mobjtblMAPI = [pscustomobject]@{
      Value  = $mobjServerHealth.MAPITest
      Status = 'none'
    }
  }

  # Add table row to report
  [array]$marrCells = $mobjtblServer, $mobjtblSite, $mobjtblRoles, $mobjtblVersion, $mobjtblDNS, $mobjtblPing, $mobjtblUptime, $mobjtblCAS, $mobjtblHT, $mobjtblMBX, $mobjtblUM, $mobjtblQueue, $mobjtblPF, $mobjtblMBX, $mobjtblMAPI
  $mstrReportData  += New-STEHTMLTableRow -Cells $marrCells
}

# Close table
$mstrReportData += New-STEHTMLTableTail
#endregion

#region MODULE SUMMARY
##################################################
# ADD MODULE SUMMARY TO REPORT SUMMARY (Universal)
##################################################
# Build summary information 
[string]$mstrModuleSummary = "
  <h3>$mstrTitle</h3>
  <ul>
"
If($marrReportSummaryData.Count -eq 0)
{
  $mstrModuleSummary += '<li><font color=#006242><strong>OK</strong></font> - No alerts or warnings</li>'
  $mstrModuleSummary += '</ul>'
}
Else
{
  ForEach ($mobjChange in $marrReportSummaryData)
  {
    $mstrModuleSummary += "<li>$mobjChange</li>"
  }
  $mstrModuleSummary += '</ul>'
}
# Save report to file
$mstrModuleSummary | Add-Content -Path "$gstrTempPath\ReportSummary.html"
#endregion

#region END MODULE
########################
# END MODULE (Universal)
########################
# Get module end time and calculate runtime
[datetime]$mdtmModuleEnd = Get-Date
[int]$mintModuleRuntime  = [math]::Round(($mdtmModuleEnd - $mdtmModuleStart).TotalMinutes)
#endregion

#region MODULE FOOTER
################################################
# ADD MODULE FOOTER TO REPORT FOOTER (Universal)
################################################
# Build footer information
[string]$mstrModuleFooter = "
  <li>$mstrTitle - Version: $mstrModuleVersion - Runtime: $mintModuleRuntime minutes</li>
"
# Add footer to report file
$mstrModuleFooter | Add-Content -Path "$gstrTempPath\ReportFooter.html"
#endregion

#region SAVE REPORT
#################################
# SAVE REPORT TO FILE (Universal)
#################################
# Remove spaces from report name
[string]$mstrReportFileName = $mstrTitle.Replace(' ','') + '_ModuleReport.html'
# Get report number
[int]$mintReportPrefix = $gintReportNumber
# Save report
$mstrReportData | Set-Content -Path "$gstrTempPath\$mintReportPrefix-$mstrReportFileName"
#endregion

#region CLEAN-UP
######################
# CLEAN-UP (Universal)
######################
# Remove module variables
Write-STELog -Level INFO -Value "$gobjModule ended! Runtime $mintModuleRuntime minutes"
Write-STELog -Level DEBUG -Value 'Finally removing variables now!'
Get-Variable -Include 'marr*', 'mdtm*', 'mhash*', 'mint*', 'mobj*', 'mstr*' | Remove-Variable -ErrorAction Continue
#endregion
