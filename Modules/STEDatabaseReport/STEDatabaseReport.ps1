﻿# Changelog:
# 3.0.16.5.9: Change database size warning level to (size - whitespace)

#region MODULE INFORMATION
################################
# MODULE INFORMATION (Universal)
################################
# Module version
[string]$mstrModuleVersion = '3.0 (Build: 3.0.16.5.9)'

# Get module start time
[datetime]$mdtmModuleStart = Get-Date
Write-STELog -Level INFO -Value "Module $gobjModule started!"
#endregion

#region INI-FILE
###############
# READ INI-FILE
###############
Try
{
  Write-STELog -Level DEBUG -Value 'Reading .ini file...'
  [hashtable]$mhashIniContent = Read-STEIniFile -Path "$gstrModulePath\$gobjModule\Settings.ini"
  Write-STELog -Level DEBUG -Value "$gobjModule.ini loaded!"
}
Catch
{
  Write-STELog -Level ERROR -Value 'Error reading .ini file'
  Write-STELog -Level ERROR -Value $Error[0]
}

# Section [ModuleSettings]
[string]$mstrTitle                = $mhashIniContent['ModuleSettings']['Title']
[int]$mintDefaultMBXDatabaseSize  = $mhashIniContent['ModuleSettings']['DefaultMBXDatabaseSize']
[int]$mintDefaultMBXCount         = $mhashIniContent['ModuleSettings']['DefaultMBXCount']
[string]$mstrExcludedMBXDatabases = $mhashIniContent['ModuleSettings']['ExcludedMBXDatabases']
# Convert string into array
[array]$marrExcludedMBXDatabases  = $mstrExcludedMBXDatabases.Split(',')

# Section [CustomMBXCount]
[hashtable]$mhashCustomMBXCount = $mhashIniContent['CustomMBXCount']

# Section [CustomDatabaseSize]
[hashtable]$mhashCustomDatabaseSize = $mhashIniContent['CustomDatabaseSize']
#endregion

#region IMPORT DATA
#############
# IMPORT DATA
#############
# Get all Exchange 2010/2013 mailbox databases
[array]$marrAllMBXDatabases = @(Get-MailboxDatabase -Status |
  Where-Object -FilterScript {
    $_.Recovery -ne $true
  } |
Sort-Object -Property Name)
# If a list of excluded mailbox databases exists, remove them from selection
If ($marrExcludedMBXDatabases.Count -gt '0')
{
  [array]$marrMBXDatabases = @()
  ForEach ($mobjDatabase in $marrAllMBXDatabases)
  {
    If ($marrExcludedMBXDatabases -notcontains $mobjDatabase)
    {
      $marrMBXDatabases += $mobjDatabase
    }
  }
}
Else
{
  $marrMBXDatabases = $marrAllMBXDatabases
}

# Get all Exchange 2010/2013 public folder databases
[array]$marrAllPublicFolderDatabases = @(Get-PublicFolderDatabase -Status -WarningAction SilentlyContinue |
Sort-Object -Property Name)

# Add mailbox and public folder databases into new array
[array]$marrAllDatabases = @()
$marrAllDatabases += $marrMBXDatabases
$marrAllDatabases += $marrAllPublicFolderDatabases
#endregion

#region START REPORT
##############
# START REPORT
##############
# Add report title
[string]$mstrReportData = New-STEModuleReport -Title $mstrTitle
# Module summary report
[array]$marrReportSummaryData = @()

# New table
[string]$mstrTableName  = 'Exchange Database Statistics'
[array]$marrTableHeader = @('Database', 'Size (GB)', 'White Space (GB)', 'Mailboxes', 'Below Limits', 'Issue Warning', 'Prohibit Send', 'Last Backup Type', 'Last Backup Time', 'Hrs Ago', 'Running')
$mstrReportData        += New-STEHTMLTable -Title $mstrTableName -Header $marrTableHeader
#endregion

#region PROCESS
#########
# PROCESS
#########
# Get information per database
ForEach ($mobjDatabase in $marrAllDatabases)
{
  If ($mobjDatabase.IsMailboxDatabase -eq $true)
  {
    # Get mailboxstatistics
    [array]$marrDBMailboxesStatistics = Get-MailboxStatistics -Database $mobjDatabase.Name |
    Where-Object -FilterScript {
      $_.DisconnectReason -ne 'SoftDeleted' -and $_.DisconnectReason -ne 'Disabled' -and $_.DisplayName -notlike '*SystemMailbox*' -and $_.DisplayName -notlike '*HealthMailbox*' -and $_.DisplayName -notlike '*Microsoft Exchange*' -and $_.DisplayName -notlike '*Discovery Search*'
    } |
    Select-Object -Property StorageLimitStatus, IsArchiveMailbox

    # Check if databases has custom settings in ['CustomMBXCount']
    If ($mhashCustomMBXCount.Keys -contains $mobjDatabase.Name)
    {
      [int]$mintMaxDBMBXCount = $mhashCustomMBXCount.Item($mobjDatabase.Name)
    }
    Else
    {
      [int]$mintMaxDBMBXCount = $mintDefaultMBXCount
    }

    # Check if databases has custom settings in ['CustomDatabaseSize']
    If ($mhashCustomDatabaseSize.Keys -contains $mobjDatabase.Name)
    {
      [int]$mintMaxDBMBXDatabaseSize = $mhashCustomDatabaseSize.Item($mobjDatabase.Name)
    }
    Else
    {
      [int]$mintMaxDBMBXDatabaseSize = $mintDefaultMBXDatabaseSize
    }

    # Count mailboxes in database
    [int]$mintMBXCount = $marrDBMailboxesStatistics.Count
  
    # Add user mailbox count to total
    [array]$marrUserMailbox = $marrDBMailboxesStatistics |  Where-Object -FilterScript {
      $_.IsArchiveMailbox -eq $false
    }
    [int]$mintUserMailboxCount       = $marrUserMailbox.Count
    [int]$mintAllUserMailboxesCount += $mintUserMailboxCount
  
    # Add archive mailbox count to total
    [array]$marrArchiveMailbox = $marrDBMailboxesStatistics |  Where-Object -FilterScript {
      $_.IsArchiveMailbox -eq $true
    }
    [int]$mintArchiveMailboxCount       = $marrArchiveMailbox.Count
    [int]$mintAllArchiveMailboxesCount += $mintArchiveMailboxCount
  
    # Find mailbox StorageLimitStatus
    [int]$mintMBXBelowLimitCount = 0
    [array]$marrMBXBelowLimit    = $marrDBMailboxesStatistics |  Where-Object -FilterScript {
      $_.StorageLimitStatus -eq 'BelowLimit'
    }
    [int]$mintMBXBelowLimitCount = $marrMBXBelowLimit.Count

    [int]$mintMBXIssueWarningCount = 0
    [array]$marrMBXIssueWarning    = $marrDBMailboxesStatistics |  Where-Object -FilterScript {
      $_.StorageLimitStatus -eq 'IssueWarning'
    }
    [int]$mintMBXIssueWarningCount = $marrMBXIssueWarning.Count

    [int]$mintMBXProhibitSendCount = 0
    [array]$marrMBXProhibitSend    = $marrDBMailboxesStatistics |  Where-Object -FilterScript {
      $_.StorageLimitStatus -eq 'ProhibitSend'
    }
    [int]$mintMBXProhibitSendCount = $marrMBXProhibitSend.Count
  }
  Else
  {
    # Check if databases has custom settings in ['CustomDatabaseSize']
    If ($mhashCustomDatabaseSize.Keys -contains $mobjDatabase.Name)
    {
      [int]$mintMaxDBMBXDatabaseSize = $mhashCustomDatabaseSize.Item($mobjDatabase.Name)
    }
    Else
    {
      [int]$mintMaxDBMBXDatabaseSize = $mintDefaultMBXDatabaseSize
    }
    [string]$mintMBXCount             = 'n/a'
    [string]$mintMBXBelowLimitCount   = 'n/a'
    [string]$mintMBXIssueWarningCount = 'n/a'
    [string]$mintMBXProhibitSendCount = 'n/a'
  }

  # BACKUP
  [hashtable]$mhashLastBackup = @{}
  [int]$mintBackupHoursAgo    = $null
  If ($mobjDatabase.LastFullBackup -eq $null -and $mobjDatabase.LastIncrementalBackup -eq $null)
  {
    # No backup timestamp was present. This means either the database has never been backed up, or it was unreachable when this script ran
    $mhashLastBackup.time       = 'Never/Unknown'
    $mhashLastBackup.type       = 'Never/Unknown'
    [string]$mintBackupHoursAgo = 'Never/Unknown'
  }
  ElseIf ($mobjDatabase.LastFullBackup -lt $mobjDatabase.LastIncrementalBackup)
  {
    # Most recent backup was Incremental
    $mhashLastBackup.time = $mobjDatabase.LastIncrementalBackup
    $mhashLastBackup.type = 'Incremental'
    
    [int]$mintBackupHoursAgo = ($mdtmModuleStart - $mhashLastBackup.time).TotalHours
    
    [int]$mintBackupHoursAgo = '{0:N0}' -f $mintBackupHoursAgo
  }
  ElseIf ($mobjDatabase.LastIncrementalBackup -lt $mobjDatabase.LastFullBackup)
  {
    # Most recent backup was Full
    $mhashLastBackup.time = $mobjDatabase.LastFullBackup
    $mhashLastBackup.type = 'Full'
    [int]$mintBackupHoursAgo = ($mdtmModuleStart - $mhashLastBackup.time).TotalHours
    [int]$mintBackupHoursAgo = '{0:N0}' -f $mintBackupHoursAgo
  }
  # Determine Yes/No status for backup in progress
  If ($($mobjDatabase.BackupInProgress) -eq $true)
  {
    [string]$mstrBackupRunning = 'Yes'
  }
  If ($($mobjDatabase.BackupInProgress) -eq $false)
  {
    [string]$mstrBackupRunning = 'No'
  }
  
  # Create custom object to store information
  $mobjMBXDatabase = [pscustomobject]@{
    Name                     = $mobjDatabase.Name
    DatabaseSize             = [math]::Round([int64](($mobjDatabase.DatabaseSize).Split('(')[1].split(' ')[0]).Replace(',','')/1024/1024/1024)
    AvailableNewMailboxSpace = [math]::Round([int64](($mobjDatabase.AvailableNewMailboxSpace).Split('(')[1].split(' ')[0]).Replace(',','')/1024/1024/1024)
    Mailboxes                = $mintMBXCount
    BelowLimits              = $mintMBXBelowLimitCount
    IssueWarning             = $mintMBXIssueWarningCount
    ProhibitSend             = $mintMBXProhibitSendCount
    LastBackupType           = $mhashLastBackup.type
    LastBackupTime           = $mhashLastBackup.time
    LastBackupHoursAgo       = $mintBackupHoursAgo
    BackupRunning            = $mstrBackupRunning
  }
  # Create custom objects for table rows
  # Database
  $mobjtblDatabase = [pscustomobject]@{
    Value  = $mobjMBXDatabase.Name
    Status = 'none'
  }
  # Size (GB)
  If ($mobjMBXDatabase.DatabaseSize -gt ($mintMaxDBMBXDatabaseSize+$mintMaxDBMBXDatabaseSize/5))
  {
    $mobjtblDBsize = [pscustomobject]@{
      Value  = $mobjMBXDatabase.DatabaseSize
      Status = 'error'
    }
    $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjMBXDatabase.Name)</strong></font> - Exceeds size limit over 20%"
  }
  ElseIf (($mobjMBXDatabase.DatabaseSize-$mobjMBXDatabase.AvailableNewMailboxSpace) -gt $mintMaxDBMBXDatabaseSize)
  {
    $mobjtblDBsize = [pscustomobject]@{
      Value  = $mobjMBXDatabase.DatabaseSize
      Status = 'warning'
    }
    $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjMBXDatabase.Name)</strong></font> - Reached size limit"
  }
  Else
  {
    $mobjtblDBsize = [pscustomobject]@{
      Value  = $mobjMBXDatabase.DatabaseSize
      Status = 'ok'
    }
  }
  # WhiteSpace (GB)
  $mobjtblWhiteSpace = [pscustomobject]@{
    Value  = $mobjMBXDatabase.AvailableNewMailboxSpace
    Status = 'none'
  }
  # Mailboxes
  If ($mintMBXCount -eq 'n/a')
  {
    $mobjtblMBXcount = [pscustomobject]@{
      Value  = 'n/a'
      Status = 'none'
    }
  }
  ElseIf ($mobjMBXDatabase.Mailboxes -gt $mintMaxDBMBXCount)
  {
    $mobjtblMBXcount = [pscustomobject]@{
      Value  = $mobjMBXDatabase.Mailboxes
      Status = 'error'
    }
    $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjMBXDatabase.Name)</strong></font> - Exceeds number of mailbox limit"
  }
  ElseIf ($mintMBXCount -gt ($mintMaxDBMBXCount-$mintMaxDBMBXCount/5))
  {
    $mobjtblMBXcount = [pscustomobject]@{
      Value  = $mobjMBXDatabase.Mailboxes
      Status = 'warning'
    }
  }
  Else
  {
    $mobjtblMBXcount = [pscustomobject]@{
      Value  = $mobjMBXDatabase.Mailboxes
      Status = 'ok'
    }
  }
  # BelowLimits
  If ($mintMBXBelowLimitCount -eq 'n/a')
  {
    $mobjtblMBXBelowLimits = [pscustomobject]@{
      Value  = 'n/a'
      Status = 'none'
    }
  }
  Else
  {
    $mobjtblMBXBelowLimits = [pscustomobject]@{
      Value  = $mobjMBXDatabase.BelowLimits
      Status = 'ok'
    }
  }
  # IssueWarning
  If ($mintMBXIssueWarningCount -eq 'n/a')
  {
    $mobjtblMBXissueWarning = [pscustomobject]@{
      Value  = 'n/a'
      Status = 'none'
    }
  }
  ElseIf ($mobjMBXDatabase.IssueWarning -gt 0)
  {
    $mobjtblMBXissueWarning = [pscustomobject]@{
      Value  = $mobjMBXDatabase.IssueWarning
      Status = 'warning'
    }
  }
  Else
  {
    $mobjtblMBXissueWarning = [pscustomobject]@{
      Value  = $mobjMBXDatabase.IssueWarning
      Status = 'ok'
    }
  }
  # ProhibitSend
  If ($mintMBXProhibitSendCount -eq 'n/a')
  {
    $mobjtblMBXprohibitSend = [pscustomobject]@{
      Value  = 'n/a'
      Status = 'none'
    }
  }
  ElseIf ($mobjMBXDatabase.ProhibitSend -gt 0)
  {
    $mobjtblMBXprohibitSend = [pscustomobject]@{
      Value  = $mobjMBXDatabase.ProhibitSend
      Status = 'error'
    }
  }
  Else
  {
    $mobjtblMBXprohibitSend = [pscustomobject]@{
      Value  = $mobjMBXDatabase.ProhibitSend
      Status = 'ok'
    }
  }
  # BACKUP
  # Last backup type
  If ($mobjMBXDatabase.LastBackupType -eq 'Never/Unknown')
  {
    $mobjtblLastBackupType = [pscustomobject]@{
      Value  = $mobjMBXDatabase.LastBackupType
      Status = 'error'
    }
  }
  Else
  {
    $mobjtblLastBackupType = [pscustomobject]@{
      Value  = $mobjMBXDatabase.LastBackupType
      Status = 'ok'
    }
  }
  # Last backup time
  If ($mobjMBXDatabase.LastBackupTime -eq 'Never/Unknown')
  {
    $mobjtblLastBackupTime = [pscustomobject]@{
      Value  = $mobjMBXDatabase.LastBackupTime
      Status = 'error'
    }
  }
  Else
  {
    $mobjtblLastBackupTime = [pscustomobject]@{
      Value  = $mobjMBXDatabase.LastBackupTime
      Status = 'ok'
    }
  }
  # Last backup hours ago
  If ($mobjMBXDatabase.LastBackupHoursAgo -lt 24)
  {
    $mobjtblLastBackupHoursAgo = [pscustomobject]@{
      Value  = $mobjMBXDatabase.LastBackupHoursAgo
      Status = 'ok'
    }
  }
  ElseIf ($mobjMBXDatabase.LastBackupHoursAgo -lt 48)
  {
    $mobjtblLastBackupHoursAgo = [pscustomobject]@{
      Value  = $mobjMBXDatabase.LastBackupHoursAgo
      Status = 'warning'
    }
    $marrReportSummaryData += "<font color=#9C6436><strong>$($mobjMBXDatabase.Name)</strong></font> - Last Backup is $($mobjMBXDatabase.LastBackupHoursAgo) hours ago"
  }
  Else
  {
    $mobjtblLastBackupHoursAgo = [pscustomobject]@{
      Value  = $mobjMBXDatabase.LastBackupHoursAgo
      Status = 'error'
    }
    $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjMBXDatabase.Name)</strong></font> - Last Backup is $($mobjMBXDatabase.LastBackupHoursAgo) hours ago"
  }
  # Backup running
  If ($mobjMBXDatabase.BackupRunning -eq 'YES')
  {
    $mobjtblBackupRunning = [pscustomobject]@{
      Value  = $mobjMBXDatabase.BackupRunning
      Status = 'ok'
    }
  }
  Else
  {
    $mobjtblBackupRunning = [pscustomobject]@{
      Value  = $mobjMBXDatabase.BackupRunning
      Status = 'none'
    }
  }

  # Add table row to report
  [array]$marrCells = $mobjtblDatabase, $mobjtblDBsize, $mobjtblWhiteSpace, $mobjtblMBXcount, $mobjtblMBXBelowLimits, $mobjtblMBXissueWarning, $mobjtblMBXprohibitSend, $mobjtblLastBackupType, $mobjtblLastBackupTime, $mobjtblLastBackupHoursAgo, $mobjtblBackupRunning
  $mstrReportData += New-STEHTMLTableRow -Cells $marrCells

  # Export trend data
  $mdtmToday               = $mdtmModuleStart.ToShortDateString()
  [string]$mstrLogFilePath = $gstrExportPath + '\' + $mobjDatabase.Name + '.csv'

  # Create custom object to export information
  $mobjMBXDatabaseExport = [pscustomobject]@{
    Date                     = $mdtmToday
    Name                     = $mobjMBXDatabase.Name
    DatabaseSize             = $mobjMBXDatabase.DatabaseSize
    AvailableNewMailboxSpace = $mobjMBXDatabase.AvailableNewMailboxSpace
    Data                     = $mobjMBXDatabase.DatabaseSize - $mobjMBXDatabase.AvailableNewMailboxSpace
    Mailboxes                = $mobjMBXDatabase.Mailboxes
  }

  $mobjMBXDatabaseExport | Export-Csv -Path $mstrLogFilePath -Append -NoTypeInformation -Delimiter ';'
}

# Close table
$mstrReportData += New-STEHTMLTableTail

# Add summary
$mstrReportData += "<p>Total number of user mailboxes: <strong>$mintAllUserMailboxesCount</strong><br>"
$mstrReportData += "Total number of archive mailboxes: <strong>$mintAllArchiveMailboxesCount</strong></p>"
#endregion

#region MODULE SUMMARY
##################################################
# ADD MODULE SUMMARY TO REPORT SUMMARY (Universal)
##################################################
# Build summary information 
[string]$mstrModuleSummary = "
  <h3>$mstrTitle</h3>
  <ul>
"
If($marrReportSummaryData.Count -eq 0)
{
  $mstrModuleSummary += '<li><font color=#006242><strong>OK</strong></font> - No alerts or warnings</li>'
  $mstrModuleSummary += '</ul>'
}
Else
{
  ForEach ($mobjChange in $marrReportSummaryData)
  {
    $mstrModuleSummary += "<li>$mobjChange</li>"
  }
  $mstrModuleSummary += '</ul>'
}
# Save report to file
$mstrModuleSummary | Add-Content -Path "$gstrTempPath\ReportSummary.html"
#endregion

#region END MODULE
########################
# END MODULE (Universal)
########################
# Get module end time and calculate runtime
[datetime]$mdtmModuleEnd = Get-Date
[int]$mintModuleRuntime  = [math]::Round(($mdtmModuleEnd - $mdtmModuleStart).TotalMinutes)
#endregion

#region MODULE FOOTER
################################################
# ADD MODULE FOOTER TO REPORT FOOTER (Universal)
################################################
# Build footer information
[string]$mstrModuleFooter = "
  <li>$mstrTitle - Version: $mstrModuleVersion - Runtime: $mintModuleRuntime minutes</li>
"
# Add footer to report file
$mstrModuleFooter | Add-Content -Path "$gstrTempPath\ReportFooter.html"
#endregion

#region SAVE REPORT
#################################
# SAVE REPORT TO FILE (Universal)
#################################
# Remove spaces from report name
[string]$mstrReportFileName = $mstrTitle.Replace(' ','') + '_ModuleReport.html'
# Get report number
[int]$mintReportPrefix = $gintReportNumber
# Save report
$mstrReportData | Set-Content -Path "$gstrTempPath\$mintReportPrefix-$mstrReportFileName"
#endregion

#region CLEAN-UP
######################
# CLEAN-UP (Universal)
######################
# Remove module variables
Write-STELog -Level INFO -Value "$gobjModule ended! Runtime $mintModuleRuntime minutes"
Write-STELog -Level DEBUG -Value 'Finally removing variables now!'
Get-Variable -Include 'marr*', 'mdtm*', 'mhash*', 'mint*', 'mobj*', 'mstr*' | Remove-Variable -ErrorAction Continue
#endregion
