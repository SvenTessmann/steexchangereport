﻿#region MODULE INFORMATION
################################
# MODULE INFORMATION (Universal)
################################
# Module version
[string]$mstrModuleVersion = '3.0 (Build: 3.0.15.6.23)'

# Get module start time
[datetime]$mdtmModuleStart = Get-Date
Write-STELog -Level INFO -Value "Module $gobjModule started!"
#endregion

#region INI-FILE
###############
# READ INI-FILE
###############
Try
{
  Write-STELog -Level DEBUG -Value 'Reading .ini file...'
  [hashtable]$mhashIniContent = Read-STEIniFile -Path "$gstrModulePath\$gobjModule\Settings.ini"
  Write-STELog -Level DEBUG -Value "$gobjModule.ini loaded!"
}
Catch
{
  Write-STELog -Level ERROR -Value 'Error reading .ini file'
  Write-STELog -Level ERROR -Value $Error[0]
}

# Section [ModuleSettings]
[string]$mstrTitle                = $mhashIniContent['ModuleSettings']['Title']
[int]$mintReplicationQueueWarning = $mhashIniContent['ModuleSettings']['ReplicationQueueWarning']
#endregion

#region IMPORT DATA
#############
# IMPORT DATA
#############
# Get all DAGs
[array]$marrAllDAGs = @(Get-DatabaseAvailabilityGroup -Status)
#endregion

#region START REPORT
##############
# START REPORT
##############
# Add report title
[string]$mstrReportData = New-STEModuleReport -Title $mstrTitle
# Module summary report
[array]$marrReportSummaryData = @()
#endregion

#region PROCESS
#########
# PROCESS
#########
If ($marrAllDAGs.Count -gt 0)
{
  ForEach ($mobjDAG in $marrAllDAGs)
  {
    # Get all DAG member server
    [array]$marrAllDAGServer = @($mobjDAG |
      Select-Object -ExpandProperty Servers |
    Sort-Object -Property Name)

    # Get all DAG databases
    [array]$marrAllDAGDatabases = @()
    $marrAllDAGDatabases = @(Get-MailboxDatabase -Status |
      Where-Object -FilterScript {
        $_.MasterServerOrAvailabilityGroup -eq $mobjDAG.Name
      } |
    Sort-Object -Property Name)

    # New table
    [string]$mstrTableName  = "$mobjDAG Database Copy Status"
    [array]$marrTableHeader = @('Database Copy', 'Database Name', 'Mailbox Server', 'Activation Preference', 'Status', 'Copy Queue', 'Replay Queue', 'Replay Lagged', 'Truncation Lagged', 'Content Index')
    $mstrReportData        += New-STEHTMLTable -Title $mstrTableName -Header $marrTableHeader

    ForEach ($mobjDatabase in $marrAllDAGDatabases)
    {
      [array]$marrDatabaseCopyStatus = @($mobjDatabase |
        Get-MailboxDatabaseCopyStatus |
      Sort-Object -Property Name)

      # Create hastable for ActivationPreference, because it is a string if called remotly
      [hashtable]$mhashActivationPreference = @{}
      ForEach($ActivationPreference in $mobjDatabase.ActivationPreference)
      {
        $ActivationPreference = $ActivationPreference.TrimStart('[').TrimEnd(']').Replace(' ','')
        $ActivationPreference = $ActivationPreference.Split(',')
        $mhashActivationPreference.Add($ActivationPreference[0],$ActivationPreference[1])
      }

      ForEach ($mobjDatabaseCopy in $marrDatabaseCopyStatus)
      {
        [string]$mstrMailboxServer     = $mobjDatabaseCopy.MailboxServer
        [int]$mintActivationPreference = $mhashActivationPreference.Get_Item($mstrMailboxServer)
       
        # Checking whether this is a replay lagged copy
        [array]$marrReplayLagCopies = @($mobjDatabase |
          Select-Object -ExpandProperty ReplayLagTimes |
          Where-Object -FilterScript {
            $_.Value -gt 0
        })
        If ($($marrReplayLagCopies.count) -gt 0)
        {
          [bool]$mblnReplayLag = $false
          ForEach ($mobjReplayLagCopy in $marrReplayLagCopies)
          {
            If ($mobjReplayLagCopy.Key -eq $mstrMailboxServer)
            {
              [bool]$mblnReplayLag = $true
            }
          }
        }
        Else
        {
          [bool]$mblnReplayLag = $false
        }

        # Checking for truncation lagged copies
        [array]$marrTruncationLagCopies = @($mobjDatabase |
          Select-Object -ExpandProperty TruncationLagTimes |
          Where-Object -FilterScript {
            $_.Value -gt 0
        })
        If ($($marrTruncationLagCopies.count) -gt 0)
        {
          [bool]$mblnTruncationLag = $false
          ForEach ($mobjTruncationLagCopy in $marrTruncationLagCopies)
          {
            If ($mobjTruncationLagCopy.Key -eq $mstrMailboxServer)
            {
              [bool]$mblnTruncationLag = $true
            }
          }
        }
        Else
        {
          [bool]$mblnTruncationLag = $false
        }

        # Create custom object for database copy to store information
        $mobjDBCopyHealth = [pscustomobject]@{
          DatabaseCopy         = $mobjDatabaseCopy.Identity
          DatabaseName         = $mobjDatabaseCopy.DatabaseName
          MailboxServer        = $mstrMailboxServer
          ActivationPreference = $mintActivationPreference
          Status               = $mobjDatabaseCopy.Status
          CopyQueue            = $mobjDatabaseCopy.CopyQueueLength
          ReplayQueue          = $mobjDatabaseCopy.ReplayQueueLength
          ReplayLagged         = $mblnReplayLag
          TruncationLagged     = $mblnTruncationLag
          ContentIndex         = $mobjDatabaseCopy.ContentIndexState
        }

        # Create custom objects for table rows
        # Database Copy
        $mobjtblDatabaseCopy = [pscustomobject]@{
          Value  = $mobjDBCopyHealth.DatabaseCopy
          Status = 'none'
        }

        # Database Name
        $mobjtblDatabaseName = [pscustomobject]@{
          Value  = $mobjDBCopyHealth.DatabaseName
          Status = 'none'
        }

        # Mailbox Server
        $mobjtblMailboxServer = [pscustomobject]@{
          Value  = $mobjDBCopyHealth.MailboxServer
          Status = 'none'
        }

        # Activation Preference
        If ($mobjDBCopyHealth.ActivationPreference -eq 1 -and $mobjDBCopyHealth.Status -eq 'Mounted')
        {
          $mobjtblActivationPreference = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.ActivationPreference
            Status = 'ok'
          }
        }
        ElseIf($mobjDBCopyHealth.ActivationPreference -gt 1 -and $mobjDBCopyHealth.Status -eq 'Healthy')
        {
          $mobjtblActivationPreference = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.ActivationPreference
            Status = 'ok'
          }
        }
        Else
        {
          $mobjtblActivationPreference = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.ActivationPreference
            Status = 'error'
          }
          $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDBCopyHealth.DatabaseName)</strong></font> - Activation Preference failure"
        }

        # Status
        If ($mobjDBCopyHealth.Status -eq 'Healthy' -or $mobjDBCopyHealth.Status -eq 'Mounted')
        {
          $mobjtblStatus = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.Status
            Status = 'ok'
          }
        }
        Else
        {
          $mobjtblStatus = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.Status
            Status = 'error'
          }
          $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDBCopyHealth.DatabaseName)</strong></font> - Status failure"
        }

        # Copy Queue
        If ($mobjDBCopyHealth.CopyQueue -lt $mintReplicationQueueWarning)
        {
          $mobjtblCopyQueue = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.CopyQueue
            Status = 'ok'
          }
        }
        Else
        {
          $mobjtblCopyQueue = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.CopyQueue
            Status = 'error'
          }
          $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDBCopyHealth.DatabaseName)</strong></font> - Copy Queue failure"
        }

        # Replay Queue
        If ($mobjDBCopyHealth.ReplayQueue -lt $mintReplicationQueueWarning)
        {
          $mobjtblReplayQueue = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.ReplayQueue
            Status = 'ok'
          }
        }
        Else
        {
          $mobjtblReplayQueue = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.ReplayQueue
            Status = 'error'
          }
          $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDBCopyHealth.DatabaseName)</strong></font> - Replay Queue failure"
        }

        # Replay Lagged
        $mobjtblReplayLagged = [pscustomobject]@{
          Value  = $mobjDBCopyHealth.ReplayLagged
          Status = 'none'
        }

        # Truncation Lagged
        $mobjtblTruncationLagged = [pscustomobject]@{
          Value  = $mobjDBCopyHealth.TruncationLagged
          Status = 'none'
        }

        # Content Index
        If ($mobjDBCopyHealth.ContentIndex -eq 'Healthy')
        {
          $mobjtblContentIndex = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.ContentIndex
            Status = 'ok'
          }
        }
        Else
        {
          $mobjtblContentIndex = [pscustomobject]@{
            Value  = $mobjDBCopyHealth.ContentIndex
            Status = 'error'
          }
          $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDBCopyHealth.DatabaseName)</strong></font> - Content Index failure"
        }

        # Add table row to report
        [array]$marrCells = $mobjtblDatabaseCopy, $mobjtblDatabaseName, $mobjtblMailboxServer, $mobjtblActivationPreference, $mobjtblStatus, $mobjtblCopyQueue, $mobjtblReplayQueue, $mobjtblReplayLagged, $mobjtblTruncationLagged, $mobjtblContentIndex
        $mstrReportData  += New-STEHTMLTableRow -Cells $marrCells
      }
    }

    # Close table
    $mstrReportData += New-STEHTMLTableTail

    # DAG MEMBER HEALTH - Get Test-Replication Health results for each DAG member
    # New table
    [string]$mstrTableName  = "$mobjDAG Member Health"
    [array]$marrTableHeader = @('Server', 'Cluster Service', 'Replay Service', 'Active Manager', 'RPC Listener', 'TCP Listener', 'Locator Service', 'Members Up', 'Cluster Network', 'Quorum Group', 'File Share Quorum', 'DB Copy Suspended', 'DB Copy Failed', 'DB Initializing', 'DB Disconnected', 'DB Log Copy Keeping Up', 'DB Log Replay Keeping Up')
    $mstrReportData        += New-STEHTMLTable -Title $mstrTableName -Header $marrTableHeader

    ForEach ($mobjServer in $marrAllDAGServer)
    {
      [array]$marrReplicationHealth = @(Test-ReplicationHealth -Server $mobjServer | Select-Object -Property Check, Result)

      # Create custom object for database to store information
      $mobjDAGMemberHealth  = [pscustomobject]@{
        Server               = $mobjServer
        ClusterService       = $marrReplicationHealth[0].Result
        ReplayService        = $marrReplicationHealth[1].Result
        ActiveManager        = $marrReplicationHealth[2].Result
        TasksRpcListener     = $marrReplicationHealth[3].Result
        TcpListener          = $marrReplicationHealth[4].Result
        ServerLocatorService = $marrReplicationHealth[5].Result
        DagMembersUp         = $marrReplicationHealth[6].Result
        ClusterNetwork       = $marrReplicationHealth[7].Result
        QuorumGroup          = $marrReplicationHealth[8].Result
        FileShareQuorum      = $marrReplicationHealth[9].Result
        DBCopySuspended      = $marrReplicationHealth[10].Result
        DBCopyFailed         = $marrReplicationHealth[11].Result
        DBInitializing       = $marrReplicationHealth[12].Result
        DBDisconnected       = $marrReplicationHealth[13].Result
        DBLogCopyKeepingUp   = $marrReplicationHealth[14].Result
        DBLogReplayKeepingUp = $marrReplicationHealth[15].Result
      }

      # Create custom objects for table rows
      # Server
      $mobjtblServer = [pscustomobject]@{
        Value  = $mobjDAGMemberHealth.Server
        Status = 'none'
      }

      # Cluster Service
      If ($mobjDAGMemberHealth.ClusterService -match 'Passed')
      {
        $mobjtblClusterService = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ClusterService
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblClusterService = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ClusterService
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - Cluster Service failure"
      }

      # Replay Service
      If ($mobjDAGMemberHealth.ReplayService -match 'Passed')
      {
        $mobjtblReplayService = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ReplayService
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblReplayService = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ReplayService
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - Replay Service failure"
      }

      # Active Manager
      If ($mobjDAGMemberHealth.ActiveManager -match 'Passed')
      {
        $mobjtblActiveManager = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ActiveManager
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblActiveManager = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ActiveManager
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - Active Manager failure"
      }

      # Tasks Rpc Listener
      If ($mobjDAGMemberHealth.TasksRpcListener -match 'Passed')
      {
        $mobjtblTasksRpcListener = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.TasksRpcListener
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblTasksRpcListener = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.TasksRpcListener
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - RPC Listener failure"
      }

      # Tcp Listener
      If ($mobjDAGMemberHealth.TcpListener -match 'Passed')
      {
        $mobjtblTcpListener = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.TcpListener
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblTcpListener = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.TcpListener
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - TCP Listener failure"
      }

      # Server Locator Service
      If ($mobjDAGMemberHealth.ServerLocatorService -match 'Passed')
      {
        $mobjtblServerLocatorService = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ServerLocatorService
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblServerLocatorService = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ServerLocatorService
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - Locator Service failure"
      }

      # Dag Members Up
      If ($mobjDAGMemberHealth.DagMembersUp -match 'Passed')
      {
        $mobjtblDagMembersUp = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DagMembersUp
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblDagMembersUp = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DagMembersUp
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - DAG Member failure"
      }

      # Cluster Network
      If ($mobjDAGMemberHealth.ClusterNetwork -match 'Passed')
      {
        $mobjtblClusterNetwork = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ClusterNetwork
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblClusterNetwork = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.ClusterNetwork
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - Cluster Network failure"
      }

      # Quorum Group
      If ($mobjDAGMemberHealth.QuorumGroup -match 'Passed')
      {
        $mobjtblQuorumGroup = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.QuorumGroup
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblQuorumGroup = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.QuorumGroup
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - Quorum Group failure"
      }

      # File Share Quorum
      If ($mobjDAGMemberHealth.FileShareQuorum -match 'Passed')
      {
        $mobjtblFileShareQuorum = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.FileShareQuorum
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblFileShareQuorum = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.FileShareQuorum
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - File Share Quorum failure"
      }

      # DB Copy Suspended
      If ($mobjDAGMemberHealth.DBCopySuspended -match 'Passed')
      {
        $mobjtblDBCopySuspended = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBCopySuspended
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblDBCopySuspended = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBCopySuspended
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - DB Copy Suspended failure"
      }

      # DB Copy Failed
      If ($mobjDAGMemberHealth.DBCopyFailed -match 'Passed')
      {
        $mobjtblDBCopyFailed = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBCopyFailed
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblDBCopyFailed = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBCopyFailed
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - DB Copy Failed failure"
      }

      # DB Initializing
      If ($mobjDAGMemberHealth.DBInitializing -match 'Passed')
      {
        $mobjtblDBInitializing = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBInitializing
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblDBInitializing = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBInitializing
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - DB Initializing failure"
      }

      # DB Disconnected
      If ($mobjDAGMemberHealth.DBDisconnected -match 'Passed')
      {
        $mobjtblDBDisconnected = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBDisconnected
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblDBDisconnected = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBDisconnected
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - DB Disconnected failure"
      }

      # DB Log Copy Keeping Up
      If ($mobjDAGMemberHealth.DBLogCopyKeepingUp -match 'Passed')
      {
        $mobjtblDBLogCopyKeepingUp = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBLogCopyKeepingUp
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblDBLogCopyKeepingUp = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBLogCopyKeepingUp
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - DB Log Copy failure"
      }

      # DB Log Replay Keeping Up
      If ($mobjDAGMemberHealth.DBLogReplayKeepingUp -match 'Passed')
      {
        $mobjtblDBLogReplayKeepingUp = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBLogReplayKeepingUp
          Status = 'ok'
        }
      }
      Else
      {
        $mobjtblDBLogReplayKeepingUp = [pscustomobject]@{
          Value  = $mobjDAGMemberHealth.DBLogReplayKeepingUp
          Status = 'error'
        }
        $marrReportSummaryData += "<font color=#9D0041><strong>$($mobjDAGMemberHealth.Server)</strong></font> - DB Log Replay failure"
      }

      # Add table row to report
      [array]$marrCells = $mobjtblServer, $mobjtblClusterService, $mobjtblReplayService, $mobjtblActiveManager, $mobjtblTasksRpcListener, $mobjtblTcpListener, $mobjtblServerLocatorService, $mobjtblDagMembersUp, $mobjtblClusterNetwork, $mobjtblQuorumGroup, $mobjtblFileShareQuorum, $mobjtblDBCopySuspended, $mobjtblDBCopyFailed, $mobjtblDBInitializing, $mobjtblDBDisconnected, $mobjtblDBLogCopyKeepingUp, $mobjtblDBLogReplayKeepingUp
      $mstrReportData  += New-STEHTMLTableRow -Cells $marrCells
    }
    # Close table
    $mstrReportData += New-STEHTMLTableTail
  }
}
Else
{
  $marrReportSummaryData += 'No database availability groups found.'
}
#endregion 

#region MODULE SUMMARY
##################################################
# ADD MODULE SUMMARY TO REPORT SUMMARY (Universal)
##################################################
# Build summary information 
[string]$mstrModuleSummary = "
  <h3>$mstrTitle</h3>
  <ul>
  "

If($marrReportSummaryData.Count -eq 0)
{
  $mstrModuleSummary += '<li><font color=#006242><strong>OK</strong></font> - No alerts or warnings</li>'
  $mstrModuleSummary += '</ul>'
}
Else
{
  ForEach ($mobjChange in $marrReportSummaryData)
  {
    $mstrModuleSummary += "<li>$mobjChange</li>"
  }
  $mstrModuleSummary  += '</ul>'
}
# Save report to file
$mstrModuleSummary | Add-Content -Path "$gstrTempPath\ReportSummary.html"
#endregion

#region END MODULE
########################
# END MODULE (Universal)
########################
# Get module end time and calculate runtime
[datetime]$mdtmModuleEnd = Get-Date
[int]$mintModuleRuntime  = [math]::Round(($mdtmModuleEnd - $mdtmModuleStart).TotalMinutes)
#endregion

#region MODULE FOOTER
################################################
# ADD MODULE FOOTER TO REPORT FOOTER (Universal)
################################################
# Build footer information
[string]$mstrModuleFooter = "
  <li>$mstrTitle - Version: $mstrModuleVersion - Runtime: $mintModuleRuntime minutes</li>
"
# Add footer to report file
$mstrModuleFooter | Add-Content -Path "$gstrTempPath\ReportFooter.html"
#endregion

#region SAVE REPORT
#################################
# SAVE REPORT TO FILE (Universal)
#################################
# Remove spaces from report name
[string]$mstrReportFileName = $mstrTitle.Replace(' ','') + '_ModuleReport.html'
# Get report number
[int]$mintReportPrefix = $gintReportNumber
# Save report
$mstrReportData | Set-Content -Path "$gstrTempPath\$mintReportPrefix-$mstrReportFileName"
#endregion

#region CLEAN-UP
######################
# CLEAN-UP (Universal)
######################
# Remove module variables
Write-STELog -Level INFO -Value "$gobjModule ended! Runtime $mintModuleRuntime minutes"
Write-STELog -Level DEBUG -Value 'Finally removing variables now!'
Get-Variable -Include 'marr*', 'mdtm*', 'mhash*', 'mint*', 'mobj*', 'mstr*' | Remove-Variable -ErrorAction Continue
#endregion
