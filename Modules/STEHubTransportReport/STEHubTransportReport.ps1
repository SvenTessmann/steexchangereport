﻿$ErrorActionPreference = 'Stop'
#region MODULE INFORMATION
################################
# MODULE INFORMATION (Universal)
################################
# Module version
[string]$mstrModuleVersion = '3.0 (Build: 3.0.15.6.23)'

# Get module start time
[datetime]$mdtmModuleStart = Get-Date
Write-STELog -Level INFO -Value "Module $gobjModule started!"
#endregion

#region INI-FILE
###############
# READ INI-FILE
###############
Try
{
  Write-STELog -Level DEBUG -Value 'Reading .ini file...'
  [hashtable]$mhashIniContent = Read-STEIniFile -Path "$gstrModulePath\$gobjModule\Settings.ini"
  Write-STELog -Level DEBUG -Value "$gobjModule.ini loaded!"
}
Catch
{
  Write-STELog -Level ERROR -Value 'Error reading .ini file'
  Write-STELog -Level ERROR -Value $Error[0]
}

# Section [ModuleSettings]
[string]$mstrTitle             = $mhashIniContent['ModuleSettings']['Title']
[string]$mstrCreateHTTrendData = $mhashIniContent['ModuleSettings']['CreateHTTrendData']
[int]$mintHTTrendPeriod        = $mhashIniContent['ModuleSettings']['HTTrendPeriod']
[string]$mstrExcludedHTServer  = $mhashIniContent['ModuleSettings']['ExcludedHTServer']
# Convert string into array
[array]$marrExcludedHTServer   = $mstrExcludedHTServer.Split(',')
#endregion

#region IMPORT DATA
#############
# IMPORT DATA
#############
# Get all Exchange 2010/2013 hub transport server
Try
{
  # Exchange 2013
  [array]$marrAllHTServer = Get-TransportService
  Write-STELog -Level DEBUG -Value 'Get Exchange 2013 hub transport server...'
}
Catch
{
  # Exchange 2010
  [array]$marrAllHTServer = Get-TransportServer
  Write-STELog -Level DEBUG -Value 'Get Exchange 2010 hub transport server...'
}
# If a list of excluded hub transport servers exists, remove them from selection
If ($marrExcludedHTServer.Count -gt '0')
{
  [array]$marrHTServer = @()
  foreach ($mobjServer in $marrAllHTServer)
  {
    if ($marrExcludedHTServer -notcontains $mobjServer)
    {
      $marrHTServer += $mobjServer
    }
  }
}
else
{
  $marrHTServer = $marrAllHTServer
}

# Get message tracking log
$mdtmMessageTrackingStart = (Get-Date -Hour 00 -Minute 00 -Second 00).AddDays(-1)
$mdtmMessageTrackingEnd   = (Get-Date -Hour 00 -Minute 00 -Second 00)
Try
{
  [array]$marrAllSendMails = $marrHTServer.Name |
  Get-MessageTrackingLog -Start $mdtmMessageTrackingStart -End $mdtmMessageTrackingEnd -EventId Send -ErrorAction SilentlyContinue -ResultSize Unlimited |
  Where-Object -FilterScript {
    $_.source -match 'SMTP' -and  $_.ConnectorId -notmatch 'Intra-Organization SMTP Send Connector' -and $_.Recipients -notmatch 'HealthMailbox' -and $_.Sender -notmatch 'MicrosoftExchange'
  } |
  Select-Object -Property ConnectorId, Totalbytes, ClientHostname, Recipients, Sender

  [array]$marrAllReceivedMails = $marrHTServer.Name |
  Get-MessageTrackingLog -Start $mdtmMessageTrackingStart -End $mdtmMessageTrackingEnd -EventId Receive -ErrorAction SilentlyContinue -ResultSize Unlimited |
  Where-Object -FilterScript {
    $_.source -match 'SMTP' -and $_.ConnectorId -notmatch 'Default' -and $_.Recipients -notmatch 'HealthMailbox' -and  $_.Recipients -notmatch 'SystemMailbox' -and $_.Sender -notmatch 'MicrosoftExchange'
  } |
  Select-Object -Property ConnectorId, Totalbytes, ServerHostname, Recipients, Sender
  Write-STELog -Level DEBUG -Value 'Get message tracking logs...'
}
Catch
{
  Write-STELog -Level ERROR -Value 'Error getting message tracking logs...'
  Write-STELog -Level ERROR -Value $Error[0]
}
#endregion

#region START REPORT
##############
# START REPORT
##############
# Add report title
[string]$mstrReportData = New-STEModuleReport -Title $mstrTitle
# Module summary report
[array]$marrReportSummaryData = @()
#endregion

#region PROCESS
#########
# PROCESS
#########
# TOTAL MAILS SEND/RECEIVED
# New table
[string]$mstrTableName  = 'Total Mails Send/Received'
[array]$marrTableHeader = @('Total Send', 'Total Received', 'Volume Send (MB)', 'Volume Received (MB)')
$mstrReportData        += New-STEHTMLTable -Title $mstrTableName -Header $marrTableHeader

$mobjTotalSendMailSize     = $marrAllSendMails | Measure-Object -Property Totalbytes -Sum
$mobjTotalReceivedMailSize = $marrAllReceivedMails | Measure-Object -Property Totalbytes -Sum

$mobjTotalSendVol = $mobjTotalSendMailSize.Sum
$mobjTotalSendVol = $mobjTotalSendVol / 1024 /1024
$mobjTotalSendVol = [math]::Round($mobjTotalSendVol)

$mobjTotalReceivedVol = $mobjTotalReceivedMailSize.Sum
$mobjTotalReceivedVol = $mobjTotalReceivedVol / 1024 /1024
$mobjTotalReceivedVol = [math]::Round($mobjTotalReceivedVol)

[int]$mintTotalSendCount     = $mobjTotalSendMailSize.Count
[int]$mintTotalReceivedCount = $mobjTotalReceivedMailSize.Count

# Create chart data
[hashtable]$hashChartDataTotalCount = @{
  Send = $mintTotalSendCount
}
$hashChartDataTotalCount += @{
  Received = $mintTotalReceivedCount
}
[hashtable]$hashChartDataTotalVol = @{
  Send = $mobjTotalSendVol
}
$hashChartDataTotalVol += @{
  Received = $mobjTotalReceivedVol
}

# Create charts
New-STECylinderChart -Title 'Total Mails Count' -Width 200 -Height 200 -Xtitle '' -Ytitle '' -Data $hashChartDataTotalCount -ImagePath "$gstrTempPath\HT_TotalMailsCount.png"
New-STECylinderChart -Title 'Total Mails Volume' -Width 200 -Height 200 -Xtitle '' -Ytitle '' -Data $hashChartDataTotalVol -ImagePath "$gstrTempPath\HT_TotalMailsVolume.png"

# Add table line
[array]$marrCells = $mintTotalSendCount, $mintTotalReceivedCount, $mobjTotalSendVol, $mobjTotalReceivedVol
$mstrReportData  += New-STEHTMLTableLine -Cells $marrCells

# Close table
$mstrReportData += New-STEHTMLTableTail

# Add charts
$mstrReportData += New-STEHTMLInlinePicture -PicturePath "$gstrTempPath\HT_TotalMails*.png"

# TOTAL MAILS BY SERVER
# New table
[string]$mstrTableName  = 'Total Mails by Server'
[array]$marrTableHeader = @('Server', 'Total Send', 'Total Received', 'Volume Send (MB)', 'Volume Received (MB)')
$mstrReportData        += New-STEHTMLTable -Title $mstrTableName -Header $marrTableHeader

ForEach ($mobjServer in $marrHTServer)
{
  $mobjServerSendMails = $marrAllSendMails | Where-Object -FilterScript {
    $_.ClientHostname -eq $mobjServer.Name
  }
  $mobjServerReceivedMails = $marrAllReceivedMails | Where-Object -FilterScript {
    $_.ServerHostname -eq $mobjServer.Name
  }

  $mobjServerTotalSendMailSize     = $mobjServerSendMails | Measure-Object -Property Totalbytes -Sum
  $mobjServerTotalReceivedMailSize = $mobjServerReceivedMails | Measure-Object -Property Totalbytes -Sum

  $mobjServerTotalSendVol = $mobjServerTotalSendMailSize.Sum
  $mobjServerTotalSendVol = $mobjServerTotalSendVol / 1024 /1024
  $mobjServerTotalSendVol = [math]::Round($mobjServerTotalSendVol)

  $mobjServerTotalReceivedVol = $mobjServerTotalReceivedMailSize.Sum
  $mobjServerTotalReceivedVol = $mobjServerTotalReceivedVol / 1024 /1024
  $mobjServerTotalReceivedVol = [math]::Round($mobjServerTotalReceivedVol)

  [int]$mintServerTotalSendCount     = $mobjServerTotalSendMailSize.Count
  [int]$mintServerTotalReceivedCount = $mobjServerTotalReceivedMailSize.Count

  # Create chart data
  $mhashChartDataServersTotalSendCount += [ordered]@{
    $mobjServer.Name = $mintServerTotalSendCount
  }
  $mhashChartDataServersTotalReceiveCount += [ordered]@{
    $mobjServer.Name = $mintServerTotalReceivedCount
  }

  # Add table line
  $marrCells       = $mobjServer, $mintServerTotalSendCount, $mintServerTotalReceivedCount, $mobjServerTotalSendVol, $mobjServerTotalReceivedVol
  $mstrReportData += New-STEHTMLTableLine -Cells $marrCells
}
# Create charts
New-STEPieChart -Title 'Total Mails Send' -Width 150 -Height 150 -Data $mhashChartDataServersTotalSendCount -ImagePath "$gstrTempPath\HT_ServerTotalSendCount.png"
New-STEPieChart -Title 'Total Mails Received' -Width 150 -Height 150 -Data $mhashChartDataServersTotalReceiveCount -ImagePath "$gstrTempPath\HT_ServerTotalReceiveCount.png"
# Close table
$mstrReportData += New-STEHTMLTableTail
# Add charts
$mstrReportData += New-STEHTMLInlinePicture -PicturePath "$gstrTempPath\HT_ServerTotal*.png"

# MAILS BY CONNECTOR
[array]$marrSendConnectorIDs    = $marrAllSendMails.ConnectorId
[array]$marrReceiveConnectorIDs = $marrAllReceivedMails.ConnectorId
[array]$marrSendConnectors      = $marrSendConnectorIDs |
Group-Object -NoElement |
Sort-Object -Property Count -Descending
[array]$marrReceiveConnectors   = $marrReceiveConnectorIDs |
Group-Object -NoElement |
Sort-Object -Property Count -Descending

# Send
# New table
[string]$mstrTableName  = 'Send Mails by Connector'
[array]$marrTableHeader = @('Connector', 'Count')
$mstrReportData        += New-STEHTMLTable -Title $mstrTableName -Header $marrTableHeader
ForEach ($mobjSendConnector in $marrSendConnectors)
{
  [string]$mstrSendConnectorName = $mobjSendConnector.Name
  [int]$mintSendConnectorCount   = $mobjSendConnector.Count

  # Add table line
  $marrCells       = $mstrSendConnectorName, $mintSendConnectorCount
  $mstrReportData += New-STEHTMLTableLine -Cells $marrCells
}
# Close table
$mstrReportData += New-STEHTMLTableTail

# Receive
# New table
[string]$mstrTableName  = 'Received Mails by Connector'
[array]$marrTableHeader = @('Connector', 'Count')
$mstrReportData        += New-STEHTMLTable -Title $mstrTableName -Header $marrTableHeader
foreach ($mobjReceiveConnector in $marrReceiveConnectors)
{
  $mobjReceiveConnectorName  = $mobjReceiveConnector.Name
  $mobjReceiveConnectorCount = $mobjReceiveConnector.Count
  # Add table line
  $marrCells       = $mobjReceiveConnectorName, $mobjReceiveConnectorCount
  $mstrReportData += New-STEHTMLTableLine -Cells $marrCells
}
# Close table
$mstrReportData += New-STEHTMLTableTail

# TREND
$mstrReportData += '<h3>Trend</h3>'
If ($mstrCreateHTTrendData -match 'YES')
{
  # Save todays data
  [string]$mstrLogDate = (Get-Date).AddDays(-1).ToShortDateString()

  [hashtable]$hashTodayDataTotalSendCount = @{
    $mstrLogDate = $mintTotalSendCount
  }
  [hashtable]$hashTodayDataTotalReceiveCount = @{
    $mstrLogDate = $mintTotalReceivedCount
  }
  $mobjTodayDataTotalSendCount    = Convert-STEHashToObject -HashTable $hashTodayDataTotalSendCount
  $mobjTodayDataTotalReceiveCount = Convert-STEHashToObject -HashTable $hashTodayDataTotalReceiveCount
  $mobjTodayDataTotalSendCount    | Export-Csv -Path "$gstrExportPath\HTReport_TrendDataSend.csv" -Append -NoTypeInformation
  $mobjTodayDataTotalReceiveCount | Export-Csv -Path "$gstrExportPath\HTReport_TrendDataReceived.csv" -Append -NoTypeInformation

  # Import trend data
  [array]$marrTrendDataTotalSendCount     = Import-Csv -Path "$gstrExportPath\HTReport_TrendDataSend.csv" |  Select-Object -Last $mintHTTrendPeriod
  [array]$marrTrendDataTotalReceivedCount = Import-Csv -Path "$gstrExportPath\HTReport_TrendDataReceived.csv" |  Select-Object -Last $mintHTTrendPeriod

  # Charting
  New-STELineChart -Title 'Mails Send per Day' -Width 500 -Height 200 -Xtitle '' -Ytitle '' -Data $marrTrendDataTotalSendCount -ImagePath "$gstrTempPath\HT_TrendMailsSendCount.png"
  New-STELineChart -Title 'Mails Received per Day' -Width 500 -Height 200 -Xtitle '' -Ytitle '' -Data $marrTrendDataTotalReceivedCount -ImagePath "$gstrTempPath\HT_TrendMailsReceiveCount.png"
  # Add charts
  $mstrReportData += New-STEHTMLInlinePicture -PicturePath "$gstrTempPath\HT_TrendMails*.png"
}
#endregion

#region MODULE SUMMARY
##################################################
# ADD MODULE SUMMARY TO REPORT SUMMARY (Universal)
##################################################
# Build summary information 
[string]$mstrModuleSummary = "
  <h3>$mstrTitle</h3>
  <ul>
"
If($marrReportSummaryData.Count -eq 0)
{
  $mstrModuleSummary += '<li><font color=#006242><strong>OK</strong></font> - No alerts or warnings</li>'
  $mstrModuleSummary += '</ul>'
}
Else
{
  ForEach ($mobjChange in $marrReportSummaryData)
  {
    $mstrModuleSummary += "<li>$mobjChange</li>"
  }
  $mstrModuleSummary   += '</ul>'
}
# Save report to file
$mstrModuleSummary | Add-Content -Path "$gstrTempPath\ReportSummary.html"
#endregion

#region END MODULE
########################
# END MODULE (Universal)
########################
# Get module end time and calculate runtime
[datetime]$mdtmModuleEnd = Get-Date
[int]$mintModuleRuntime  = [math]::Round(($mdtmModuleEnd - $mdtmModuleStart).TotalMinutes)
#endregion

#region MODULE FOOTER
################################################
# ADD MODULE FOOTER TO REPORT FOOTER (Universal)
################################################
# Build footer information
[string]$mstrModuleFooter = "
  <li>$mstrTitle - Version: $mstrModuleVersion - Runtime: $mintModuleRuntime minutes</li>
"
# Add footer to report file
$mstrModuleFooter | Add-Content -Path "$gstrTempPath\ReportFooter.html"
#endregion

#region SAVE REPORT
#################################
# SAVE REPORT TO FILE (Universal)
#################################
# Remove spaces from report name
[string]$mstrReportFileName = $mstrTitle.Replace(' ','') + '_ModuleReport.html'
# Get report number
[int]$mintReportPrefix = $gintReportNumber
# Save report
$mstrReportData | Set-Content -Path "$gstrTempPath\$mintReportPrefix-$mstrReportFileName"
#endregion

#region CLEAN-UP
######################
# CLEAN-UP (Universal)
######################
# Remove module variables
Write-STELog -Level INFO -Value "$gobjModule ended! Runtime $mintModuleRuntime minutes"
Write-STELog -Level DEBUG -Value 'Finally removing variables now!'
Get-Variable -Include 'marr*', 'mdtm*', 'mhash*', 'mint*', 'mobj*', 'mstr*' | Remove-Variable -ErrorAction Continue
#endregion