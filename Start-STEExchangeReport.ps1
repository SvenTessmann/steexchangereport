﻿#Requires -Version 3.0

#region INFORMATION
################################
# SCRIPT INFORMATION (Universal)
################################
# Script version and author
[string]$gstrScriptVersion = '3.0 (Build: 3.0.15.4.11)'
[string]$gstrScriptAuthor  = 'Sven Tessmann'

# Get local computername
[string]$gstrScriptServer = $env:COMPUTERNAME

# Get script start date/time
[datetime]$gdtmScriptStart = Get-Date
#endregion

#region PATHS
############################
# BUILDING PATHS (Universal)
############################
# Get the current location (path where this script is located)
[string]$gstrInstallPath = Get-Location | Select-Object -ExpandProperty Path

# Building paths to subfolders
Try
{
  Write-Host -Object 'Building needed paths...' -ForegroundColor Cyan
  [string]$gstrArchivePath  = "$gstrInstallPath\Archive"
  [string]$gstrExportPath   = "$gstrInstallPath\Export"
  [string]$gstrFunctionPath = "$gstrInstallPath\Functions"
  [string]$gstrHelpPath     = "$gstrInstallPath\Help"
  [string]$gstrImportPath   = "$gstrInstallPath\Import"
  [string]$gstrLogPath      = "$gstrInstallPath\Logs"
  [string]$gstrModulePath   = "$gstrInstallPath\Modules"
  [string]$gstrTempPath     = "$gstrInstallPath\Temp"
}
Catch
{
  Write-Host -Object $Error[0] -ForegroundColor Red
  exit 0
}
#endregion

#region MAIN FUNCTIONS
#######################################
# INITIALIZE MAIN FUNCTIONS (Universal)
#######################################
# Path to general functions
$gstrGeneralFunctionPath = "$gstrFunctionPath\General"

# Load general functions
Try
{
  Write-Host -Object 'Loading available main functions...' -ForegroundColor Cyan
  [array]$garrAllFunctions = Get-ChildItem -Path $gstrGeneralFunctionPath -Recurse -ErrorAction Stop | Select-Object -ExpandProperty Name
}
Catch
{
  Write-Host -Object 'Is the script executed from the correct path?' -ForegroundColor Red
  Write-Host -Object $Error[0] -ForegroundColor Red
  exit 0
}

ForEach($gobjFunction in $garrAllFunctions)
{
  Try
  {
    . "$gstrGeneralFunctionPath\$gobjFunction"
  }
  Catch
  {
    Write-Host -Object $Error[0] -ForegroundColor Red
    exit 0
  }
}
#endregion

#region INI-FILE
###########################
# READ INI-FILE (Universal)
###########################
Try
{
  Write-Host -Object 'Reading .ini file...' -ForegroundColor Cyan
  [hashtable]$ghashIniContent = Read-STEIniFile -Path "$gstrInstallPath\Settings.ini"

  # Section [GeneralSettings]
  [string]$gstrTitle          = $ghashIniContent['GeneralSettings']['Title']
  [string]$gstrCustomer       = $ghashIniContent['GeneralSettings']['Customer']
  [string]$gintLogLevel       = $ghashIniContent['GeneralSettings']['LogLevel']
  [string]$gstrCreateReport   = $ghashIniContent['GeneralSettings']['CreateReport']
  [string]$gstrArchiveReport  = $ghashIniContent['GeneralSettings']['ArchiveReport']
  [string]$gstrUseCharting    = $ghashIniContent['GeneralSettings']['UseCharting']
  [string]$gstrSendMail       = $ghashIniContent['GeneralSettings']['SendMail']
  [string]$gstrEnableEMS      = $ghashIniContent['GeneralSettings']['EnableEMS']
  [string]$gstrExchangeServer = $ghashIniContent['GeneralSettings']['ExchangeServer']

  # Section [SMTPSettings]
  [string]$gstrRecipients     = $ghashIniContent['SMTPSettings']['Recipients']
  [string]$gstrSender         = $ghashIniContent['SMTPSettings']['Sender']
  [string]$gstrSubject        = $ghashIniContent['SMTPSettings']['Subject']
  [string]$gstrSMTPServer     = $ghashIniContent['SMTPSettings']['SMTPserver']
  [string]$gstrSMTPServerAuth = $ghashIniContent['SMTPSettings']['SMTPServerAuth']
  If ($gstrSMTPServerAuth -match 'YES')
  {
    [string]$gstrSMTPUser     = $ghashIniContent['SMTPSettings']['SMTPUser']
    [string]$gstrSMTPPassword = $ghashIniContent['SMTPSettings']['SMTPPassword']
    $gobjSecurePassword       = ConvertTo-SecureString -String $gstrSMTPPassword -AsPlainText -Force
    $gobjSMTPCredentials      = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList ($gstrSMTPUser, $gobjSecurePassword)
  }

  # Section [Modules]
  [hashtable]$ghashModules = $ghashIniContent['Modules']
  # Sort modules by key and export values into array
  [array]$garrModules = @(($ghashModules.GetEnumerator() | Sort-Object -Property Name).Value)
}
Catch
{
  Write-Host -Object $Error[0] -ForegroundColor Red
  exit 0
}
#endregion

#region LOGGING
################################
# INITIALIZE LOGGING (Universal)
################################
[string]$gstrLogFile = "$gstrLogPath\logfile.log"
Start-STELog
Write-STELog -Level DEBUG -Value 'Started logging...'
#endregion

#region CHARTING
###########################################
# INITIALIZE CHARTING FUNCTIONS (Universal)
###########################################
If($gstrUseCharting -eq 'YES')
{
  Try
  {
    Write-STELog -Level DEBUG -Value 'Loading .NET assemblies for charting...'
    # Load the appropriate assemblies for charts
    Try
    {
      [void][Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')
      Write-STELog -Level DEBUG -Value 'System.Windows.Forms loaded'
    }
    Catch
    {
      Write-STELog -Level ERROR -Value 'Error loading System.Windows.Forms'
      Write-STELog -Level ERROR -Value $Error[0]
    }
    Try
    {
      [void][Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms.DataVisualization')
      Write-STELog -Level DEBUG -Value 'System.Windows.Forms.DataVisualization loaded'
    }
    Catch
    {
      Write-STELog -Level ERROR -Value 'Error loading System.Windows.Forms.DataVisualization'
      Write-STELog -Level ERROR -Value $Error[0]
    }
  }
  Catch
  {
    Write-STELog -Level ERROR -Value 'Error loading .NET assemblies for charting'
    Write-STELog -Level ERROR -Value $Error[0]
    exit 0
  }
  
  # Path to charting functions
  $gstrChartingFunctionPath = "$gstrFunctionPath\Charting"
  
  # Load charting functions
  Try
  {
    Write-STELog -Level DEBUG -Value 'Loading available charting functions...'
    [array]$garrAllChartingFunctions = Get-ChildItem -Path $gstrChartingFunctionPath -Recurse | Select-Object -ExpandProperty Name

    ForEach($gobjChartingFunction in $garrAllChartingFunctions)
    {
      Try
      {
        . "$gstrChartingFunctionPath\$gobjChartingFunction"
        Write-STELog -Level DEBUG -Value "$gobjChartingFunction loaded"
      }
      Catch
      {
        Write-STELog -Level ERROR -Value "Error loading $gobjChartingFunction"
        Write-STELog -Level ERROR -Value $Error[0]
        exit 0
      }
    }
  }
  Catch
  {
    Write-STELog -Level ERROR -Value 'Error loading charting functions'
    Write-STELog -Level ERROR -Value $Error[0]
    exit 0
  }
}
#endregion

#region EXCHANGE
###########################################
# INITIALIZE EXCHANGE FUNCTIONS (Universal)
###########################################
If($gstrEnableEMS -eq 'YES')
{
  # Path to Exchange functions
  $gstrExchangeFunctionPath = "$gstrFunctionPath\Exchange"
  
  # Load Exchange functions 
  Try
  {
    Write-STELog -Level DEBUG -Value 'Loading available exchange functions...'
    [array]$garrAllExchangeFunctions = Get-ChildItem -Path $gstrExchangeFunctionPath -Recurse | Select-Object -ExpandProperty Name

    ForEach($gobjExchangeFunction in $garrAllExchangeFunctions)
    {
      Try
      {
        . "$gstrExchangeFunctionPath\$gobjExchangeFunction"
        Write-STELog -Level DEBUG -Value "$gobjExchangeFunction loaded"
      }
      Catch
      {
        Write-STELog -Level ERROR -Value "Error loading $gobjExchangeFunction"
        Write-STELog -Level ERROR -Value $Error[0]
        exit 0
      }
    }

    # Connect to Exchange using implicit remoting
    Try
    {
      Write-STELog -Level DEBUG -Value 'Connect to Exchange session...'
      Connect-STEExchangeServer -ExchangeServer $gstrExchangeServer
      Write-STELog -Level DEBUG -Value "Connected to $gstrExchangeServer"
    }
    Catch
    {
      Write-STELog -Level ERROR -Value "Error connecting to server $gstrExchangeServer"
      Write-STELog -Level ERROR -Value $Error[0]
      exit 0
    }
    
    # Set scope to include entire forest
    Try
    {
      If ((Get-ADServerSettings).ViewEntireForest -eq $False)
      {
        Set-ADServerSettings -ViewEntireForest $True -WarningAction SilentlyContinue
        Write-STELog -Level DEBUG -Value 'Set ADServerSettings to ViewEntireForest'
      }
    }
    Catch
    {
      Write-STELog -Level ERROR -Value 'Error setting ADServerSettings to ViewEntireForest'
      Write-STELog -Level ERROR -Value $Error[0]
    }
  }
  Catch
  {
    Write-STELog -Level ERROR -Value 'Error loading Exchange functions'
    Write-STELog -Level ERROR -Value $Error[0]
  }
}
#endregion

#region REPORT START
###############################
# START MAIN REPORT (Universal)
###############################
If($gstrCreateReport -eq 'YES')
{
  Try
  {
    Write-STELog -Level DEBUG -Value 'Generate main reports...'

    # Create main report
    [string]$strAddToHTMLReport = New-STEMainReport -Title $gstrTitle
    $strAddToHTMLReport | Set-Content -Path "$gstrTempPath\MainReport.html"
    Write-STELog -Level DEBUG -Value 'MainReport.html created'

    # Create main summary report
    $strAddToHTMLReport = New-STEModuleReport -Title 'Report Summary'
    $strAddToHTMLReport | Set-Content -Path "$gstrTempPath\ReportSummary.html"
    Write-STELog -Level DEBUG -Value 'ReportSummary.html created'
  
    # Create main footer
    $strAddToHTMLReport = New-STEReportFooter
    $strAddToHTMLReport | Set-Content -Path "$gstrTempPath\ReportFooter.html"
    Write-STELog -Level DEBUG -Value 'ReportFooter.html created'
  }
  Catch
  {
    Write-STELog -Level ERROR -Value 'Error creating report files'
    Write-STELog -Level ERROR -Value $Error[0]
    exit 0
  }
}
#endregion

#region MODULES
#############################
# RUNNING MODULES (Universal)
#############################
# Run module scripts
[int]$gintReportNumber = '1'
ForEach ($gobjModule in $garrModules)
{
  Try 
  {
    Write-STELog -Level DEBUG -Value "Processing Module $gobjModule..."
    . "$gstrModulePath\$gobjModule\$gobjModule.ps1"
  }
  Catch
  {
    Write-STELog -Level ERROR -Value "Error processing $gobjModule"
    Write-STELog -Level ERROR -Value $Error[0]
  }
  $gintReportNumber++
}
#endregion

#region BUILD REPORT
###############################
# BUILD MAIN REPORT (Universal)
###############################
If($gstrCreateReport -eq 'YES')
{
  # Add summary report to main report
  $strAddToHTMLReport = Get-Content -Path "$gstrTempPath\ReportSummary.html"
  $strAddToHTMLReport | Add-Content -Path "$gstrTempPath\MainReport.html"

  # Add module reports to main report
  [array]$garrModuleReportFiles = @((Get-ChildItem -Path "$gstrTempPath\*_ModuleReport.html" | Sort-Object -Property Name).Name)
  ForEach($ModuleReportFile in $garrModuleReportFiles)
  {
    $strAddToHTMLReport = Get-Content -Path $gstrTempPath\$ModuleReportFile
    $strAddToHTMLReport | Add-Content -Path "$gstrTempPath\MainReport.html"
  }

  # Add footer to main report
  $strAddToHTMLReport = Get-Content -Path "$gstrTempPath\ReportFooter.html"
  $strAddToHTMLReport | Add-Content -Path "$gstrTempPath\MainReport.html"

  # Add tail to report
  $strAddToHTMLReport = New-STEReportTail
  $strAddToHTMLReport | Add-Content -Path "$gstrTempPath\MainReport.html"

  # Archive report
  If($gstrArchiveReport -eq 'YES')
  {
    Write-STELog -Level DEBUG -Value 'Archive report...'
    Try
    {
      [string]$gstrReportArchivePrefix = Get-Date -Format yyyy.MM.dd
      [string]$gstrReportArchiveName   = '_MainReport.html'
      Copy-Item -Path "$gstrTempPath\MainReport.html" -Destination "$gstrArchivePath\$gstrReportArchivePrefix$gstrReportArchiveName"
      Write-STELog -Level DEBUG -Value "MainReport.html archived to $gstrArchivePath\$gstrReportArchivePrefix$gstrReportArchiveName"
    }
    Catch
    {
      Write-STELog -Level ERROR -Value "Error archiving $gstrReportArchiveName"
      Write-STELog -Level ERROR -Value $Error[0]
    }
  }
}
#endregion

#region E-MAIL
############################
# SENDING E-MAIL (Universal)
############################
If($gstrSendMail -eq 'YES')
{
  Try 
  {
    Write-STELog -Level DEBUG -Value 'Sending report...'
    $gobjMailbody       = Get-Content -Path "$gstrTempPath\MainReport.html" | Out-String
    $gobjAllAttachments = Get-ChildItem -Path "$gstrTempPath\*.png"

    # If exists convert attachments into hashtable
    If ($gobjAllAttachments)
    {
      [hashtable]$ghashMailAttachments = @{}
      ForEach($gobjAttachment in $gobjAllAttachments)
      {
        $ghashMailAttachments.Add($gobjAttachment.Name, $gobjAttachment.FullName)
      }
      If ($gstrSMTPServerAuth -match 'YES')
      {
        Send-STEMail -From "$gstrSender" -To "$gstrRecipients" -Subject "$gstrSubject" -SmtpServer $gstrSMTPServer -BodyAsHtml -Body $gobjMailbody -InlineAttachments $ghashMailAttachments -Credential $gobjSMTPCredentials
        Write-STELog -Level DEBUG -Value "$gstrSubject send with attachment and authentication"
      }
      Else
      {
        Send-STEMail -From "$gstrSender" -To "$gstrRecipients" -Subject "$gstrSubject" -SmtpServer $gstrSMTPServer -BodyAsHtml -Body $gobjMailbody -InlineAttachments $ghashMailAttachments
        Write-STELog -Level DEBUG -Value "$gstrSubject send with attachment but without authentication"
      }
    }

    # No attachments exists
    Else
    {
      If ($gstrSMTPServerAuth -match 'YES')
      {
        Send-STEMail -From "$gstrSender" -To "$gstrRecipients" -Subject "$gstrSubject" -SmtpServer $gstrSMTPServer -BodyAsHtml -Body $gobjMailbody -Credential $gobjSMTPCredentials
        Write-STELog -Level DEBUG -Value "$gstrSubject send without attachment but with authentication"
      }
      Else
      {
        Send-STEMail -From "$gstrSender" -To "$gstrRecipients" -Subject "$gstrSubject" -SmtpServer $gstrSMTPServer -BodyAsHtml -Body $gobjMailbody
        Write-STELog -Level DEBUG -Value "$gstrSubject send without attachment and without authentication"
      }
    }
  }
  Catch
  {
    Write-STELog -Level ERROR -Value 'Error sending report'
    Write-STELog -Level ERROR -Value $Error[0]
  }
}
#endregion

#region CLEAN-UP
######################
# CLEAN-UP (Universal)
######################
# Delete temporary files
Write-STELog -Level DEBUG -Value 'Removing temporary files...'
Try
{
  Get-ChildItem -Path $gstrTempPath | Remove-Item
  Write-STELog -Level DEBUG -Value 'Temporary files removed'
}
Catch
{
  Write-STELog -Level ERROR -Value 'Error removing temporary files'
  Write-STELog -Level ERROR -Value $Error[0]
}

# Get report end time
[datetime]$gdtmScriptEnd = Get-Date

# Close Log
Close-STELog

# Remove any PSSession
Get-PSSession | Remove-PSSession
#endregion